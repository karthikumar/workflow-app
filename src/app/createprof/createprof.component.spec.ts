import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateprofComponent } from './createprof.component';

describe('CreateprofComponent', () => {
  let component: CreateprofComponent;
  let fixture: ComponentFixture<CreateprofComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateprofComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateprofComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';



@Component({
  selector: 'app-createprof',
  templateUrl: './createprof.component.html',
  styleUrls: ['./createprof.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CreateprofComponent implements OnInit {

  public addrDetails;
  public eduDetails;
  @ViewChild('stepperHor') stepperHor: MatStepper;

  constructor(private _formBuilder: FormBuilder,) { }

  ngOnInit(): void {

    this.addrDetails = this._formBuilder.group({
      ownerType: ['', Validators.required],

    });
    this.eduDetails = this._formBuilder.group({
      schl: ['', Validators.required],

    });
  }
  ngAfterViewInit() {
    this.stepperHor.selectedIndex = 1;
  }
}

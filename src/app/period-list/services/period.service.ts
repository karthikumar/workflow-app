import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PeriodService {

  constructor(
    private http: HttpClient) { }
  
  GetServiceWorkflows(serviceId) {
    return this.http.get<any>('http://localhost:44339/api/service/' + serviceId + '/workflows');
  }
  
  GetServiceDetails(serviceId) {
    return this.http.get<any>('http://localhost:44339/api/service/' + serviceId);
  }


  updateAdminPeriodData(data) {
    return this.http.post<any>(`http://localhost:44339/api/service/period/update`, data);
  }

  cloneWorkFlow(data) {
    return this.http.post<any>(`http://localhost:44339/api/workflow/clone`, data);
  }
}

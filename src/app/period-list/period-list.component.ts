import { Component, OnInit, ViewChild } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { PeriodService } from './services/period.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-period-list',
  templateUrl: './period-list.component.html',
  styleUrls: ['./period-list.component.scss']
})
export class PeriodListComponent implements OnInit {
  public serviceform: FormGroup;
  public periodform: FormGroup;
  public singlePeriod: FormGroup;
  public periodupdateform: FormArray;
  public serviceMsg: any;
  public serviceErrMsg: any;
  public adminServiceRes: any;
  public periodDetails: any;
  public serviceWorkflows: any;
  public showCloneIndex: number = -1;
  options = [
    { value: 'Option one' }, { value: 'Option two' }, { value: 'Option three' }
  ]
  constructor(
    private fb: FormBuilder,
    private periodService: PeriodService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.serviceform = this.fb.group({
      serviceName: [''],
      groupName: [''],
      isBranchAllocatable: [false],
      isRecurring: [false],
      isNegotiable: [false],
      isRecheckRequired: [false]
    }),
      this.periodupdateform = this.fb.array([]),
      this.periodform = this.fb.group({
        period: this.fb.array([])
      })
  }

  public showNewPeriodField: boolean = false;
  public showAdminServForm: boolean = true;
  periodDt1: FormControl;

  ngOnInit(): void {
    this.periodDt1 = new FormControl(new Date())
    // this.addPeriod();
    this.periodService.GetServiceDetails(this.route.snapshot.paramMap.get('id')).subscribe(
      (response: any) => {
        console.log(response)
        if (response.success == true) {
          this.serviceMsg = response.message;
          this.showAdminServForm = false;
          this.serviceErrMsg = null;
          this.adminServiceRes = response.data;
          this.getPeriodDetails();
          this.GetServiceWorkflows();
        } else {
          this.serviceErrMsg = response.message;
          this.serviceMsg = null;
          this.showAdminServForm = true;
        }
        //this.loading = false;
      },
      (error) => {
        //this.loading = false;
        this.serviceErrMsg = error.error.message;
        this.serviceMsg = null;
        this.showAdminServForm = true;
      });
  }
  get periodForms() {
    return this.periodform.get('period') as FormArray
  }

  addPeriod() {

    const period = this.fb.group({
      name: [],
      dueDate: [],
      sequence: [],
    })

    this.periodForms.push(period);
  }

  deletePeriod(i) {
    this.periodForms.removeAt(i);
  }
  showNewPeriod(val) {
    if (val == 0) {
      this.showNewPeriodField = true;
    } else {
      this.showNewPeriodField = false;
    }
  }

  adminFunc() {
    this.showAdminServForm = false;
  }

  getPeriodDetails() {
    this.periodDetails = this.adminServiceRes.periods;
    this.periodDetails.forEach(element => {
      this.periodupdateform.controls.push(this.fb.group({
        name: element.name,
        dueDate: element.dueDate,
        sequence: element.sequence,
        periodId: element.id,
        cloneName: '',
        workflowId: element.workflowId,
        workflowName: element.workFlowName,
        expiryDate: element.expiryDate
      }));
    });

  }
  readOnly = true;
  editFn() {
    this.readOnly = false;
  }

  showCloneFn(index) {
    this.showCloneIndex = index;
  }
  applyClone(index) {
    let changedData = this.periodupdateform.controls[index].value;
    let data = {
      workflowId: changedData.workflowId,
      name: changedData.cloneName,
      periodId: changedData.periodId
    }
    this.periodService.cloneWorkFlow(data).subscribe(
      (response: any) => {
        console.log(response)
        if (response.success == true) {
          this.serviceMsg = response.message;
          this.CancelFn();
          this.GetServiceWorkflows();
        } else {
          this.serviceErrMsg = response.message;
          this.serviceMsg = null;
        }
      },
      (error) => {
        //this.loading = false;
        this.serviceErrMsg = error.error.message;
        this.serviceMsg = null;
      });
  }
  CancelFn() {
    this.showCloneIndex = -1;
    this.readOnly = true;
    this.serviceErrMsg = null;
  }
  AddFn(index) {
    let changedData = this.periodupdateform.controls[index].value;
    this.router.navigateByUrl('/workflow/' + changedData.periodId);
  }
  updateFn(index) {
    let changedData = this.periodupdateform.controls[index].value;
    console.log(changedData)
    let data = {
      name: changedData.name,
      id: changedData.periodId,
      dueDate: changedData.dueDate,
      sequence: Number(changedData.sequence),
      expiryDate: changedData.expiryDate
    }
    this.periodService.updateAdminPeriodData(data).subscribe(
      (response: any) => {
        console.log(response)
        if (response.success == true) {
          this.serviceMsg = response.message;
          this.CancelFn();
        } else {
          this.serviceErrMsg = response.message;
          this.serviceMsg = null;
        }
      },
      (error) => {
        //this.loading = false;
        this.serviceErrMsg = error.error.message;
        this.serviceMsg = null;
      });
  }
  GetServiceWorkflows() {
    // 
    this.periodService.GetServiceWorkflows(this.adminServiceRes.id).subscribe(
      (response: any) => {
        if (response.success == true) {
          this.serviceWorkflows = response.data.workflows;
        }
      });
  }
}

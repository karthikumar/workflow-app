import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { SleekWorkflowDialogComponent } from './sleek-workflow-dialog/sleek-workflow-dialog.component';

@Component({
  selector: 'app-sleek-workflow',
  templateUrl: './sleek-workflow.component.html',
  styleUrls: ['./sleek-workflow.component.scss']
})
export class SleekWorkflowComponent implements OnInit {

  public addrDetails;

  constructor(private formBuilder: FormBuilder,
    public dialog: MatDialog, ) { }

  openDialog(arg) {
    
    const dialogRef = this.dialog.open(SleekWorkflowDialogComponent, {
      width: '60%',
      maxHeight: 'calc(100vh - 90px)',
      height : 'auto',
    });
    dialogRef.afterClosed().subscribe(result => {
    });
    dialogRef.componentInstance.view = arg;

  }

  ngOnInit(): void {
    this.addrDetails = this.formBuilder.group({
      ownerType: ['', Validators.required],

    });
  }

}

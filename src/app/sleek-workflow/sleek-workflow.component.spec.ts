import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SleekWorkflowComponent } from './sleek-workflow.component';

describe('SleekWorkflowComponent', () => {
  let component: SleekWorkflowComponent;
  let fixture: ComponentFixture<SleekWorkflowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SleekWorkflowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SleekWorkflowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

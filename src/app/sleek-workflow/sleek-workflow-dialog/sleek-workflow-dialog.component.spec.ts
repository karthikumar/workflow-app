import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SleekWorkflowDialogComponent } from './sleek-workflow-dialog.component';

describe('SleekWorkflowDialogComponent', () => {
  let component: SleekWorkflowDialogComponent;
  let fixture: ComponentFixture<SleekWorkflowDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SleekWorkflowDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SleekWorkflowDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

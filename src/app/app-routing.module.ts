import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CreateprofComponent } from './createprof/createprof.component';
import { VerifyUserComponent } from './verify-user/verify-user.component';
import { SetPasswordComponent } from './set-password/set-password.component';

import { TaskComponent } from './task/task.component';
import { AuthGuardService as AuthGuard } from '../app/services/auth-guard.service';
import { AdminserviceComponent } from './adminservice/adminservice.component';
import { WorkflowComponent } from './workflow/workflow.component';
import { PeriodListComponent } from './period-list/period-list.component';
import { AdminServiceEditComponent } from './admin-service-edit/admin-service-edit.component';
import { FeesChartComponent } from './fees-chart/fees-chart.component';
import { ClientListComponent } from './client-list/client-list.component';
import { ClientEntitiesComponent } from './client-list/client-entities/client-entities.component';
import { EntityBranchComponent } from './client-list/entity-branch/entity-branch.component';
import { BankComponent } from './bank/bank.component';
import { QuotationComponent } from './quotation/quotation.component';
import { SleekWorkflowComponent } from './sleek-workflow/sleek-workflow.component';
import { ServicesLandingComponent } from './services-landing/services-landing.component';

const routes: Routes = [
  { path: 'setPassword', component: SetPasswordComponent, canActivate: [AuthGuard] },
  { path: 'verifyUser', component: VerifyUserComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'createprofile', component: CreateprofComponent },
  { path: 'feeschart', component: FeesChartComponent },
  { path: 'clientlist', component: ClientListComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'task/:id/:taskid/:allocationId/:isAdministrative', component: TaskComponent, canActivate: [AuthGuard] },
  { path: 'createprofile', component: CreateprofComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'service/add', component: AdminserviceComponent },
  { path: 'service/:id', component: PeriodListComponent },
  { path: 'workflow/:serviceid', component: WorkflowComponent },
  { path: 'service/edit/:id', component: AdminServiceEditComponent },
  { path: 'workflow/:serviceid', component: WorkflowComponent},
  { path: 'workflow/:serviceid/:workflowid', component: WorkflowComponent},
  { path: 'cliententity/:cid', component: ClientEntitiesComponent },
  { path: 'entitybranch/:cid/:EntId', component: EntityBranchComponent },
  { path: 'entitybank/:cid/:branchId', component: BankComponent },
  { path: 'quotation', component: QuotationComponent },
  { path: 'sleekworkflow', component: SleekWorkflowComponent },
  { path: 'services', component: ServicesLandingComponent },
  { path: '', component: DashboardComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ServiceService } from './service.service';
import { Router } from '@angular/router';
import { AngularTreeGridComponent } from 'angular-tree-grid';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-services-landing',
  templateUrl: './services-landing.component.html',
  styleUrls: ['./services-landing.component.scss']
})
export class ServicesLandingComponent implements OnInit {
  // @ViewChild('MatPaginator') paginator: MatPaginator;
  public triggerMsg: any;
  public services: any;
  public workflowform: FormGroup;
  configuredServices: any = [];
  showloader: boolean = true;
  workflowSelectShow: boolean = false;
  isWorkflowTriggered: boolean = false;
  serviceSuccessMsg: string;
  public workflows: any;
  @ViewChild('angularGrid') angularGrid: AngularTreeGridComponent;
  data: any = [];
  configs: any = {
    id_field: 'id',
    parent_id_field: 'parent',
    parent_display_field: 'name',
    css: { // Optional
      expand_class: 'closed',
      collapse_class: 'opened',
    },
    columns: [
      {
        name: 'name',
        header: 'Workflow tree',
        width: '400px'
      },
      {
        name: 'seq',
        header: '',
        renderer: function (value) {
          if (value) {
            return "Sequence No. " + value;
          } else {
            return '';
          }
        }
      },
      {
        name: 'serviceIdToEdit',
        header: '',
        width: '10%',
        renderer: function (value) {
          if (value) {
            return '<a href="javascript:void(0);">Edit</a>';
          } else {
            return '';
          }
        }
      },
      {
        name: 'serviceIdToCreate',
        header: '',
        width: '15%',
        renderer: function (value) {
          if (value) {
            return '<a href="javascript:void(0);">Create Workflow</a>';
          } else {
            return '';
          }
        }
      },
      {
        name: 'workflowId',
        header: '',
        width: '10%',
        renderer: function (value) {
          if (value) {
            return '<a href="javascript:void(0);">Edit</a>';
          } else {
            return '';
          }
        }
      },
      {
        name: 'triggerWorkflowId',
        header: '',
        width: '10%',
        renderer: function (value) {
          if (value) {
            return '<a href="javascript:void(0);">Trigger</a>';
          } else {
            return '';
          }
        }
      }
    ]
  };

  constructor(
    private serviceService: ServiceService,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.workflowform = this.fb.group({
      service: ['', Validators.required],
      workflow: ['', Validators.required],
    })
  }
  ngOnInit(): void {
    this.GetServiceDetails();
    this.showloader = false;
  }
  private GetServiceDetails() {
    this.serviceService.getServices().subscribe(
      (response: any) => {
        this.services = response.data;
        let i = 1;
        response.data.forEach(element => {
          let service = {
            id: i,
            name: element.name,
            workflowName: '',
            parent: 0,
            seq: '',
            serviceIdToCreate: element.id,
            serviceIdToEdit: element.id
          };
          this.configuredServices.push(service);
          let j = i + 1;
          element.workflows.forEach(period => {
            let service = {
              id: j,
              name: "Associated Workflow: " + period.name,
              workflowId: element.id+"/"+period.id,
              parent: i,
              triggerWorkflowId : period.id
            };
            this.configuredServices.push(service);
            j = j + 1;
          });
          i = j;
          this.showloader = false;
          i = i + 1;
        });
        this.data = this.configuredServices;
      });
  }

  redirectToAnotherPage(data) {
    if(data.column.name=='serviceIdToCreate')
    {
      this.router.navigateByUrl('/workflow/' + data.row.serviceIdToCreate);
    } 
    else if(data.column.name=='serviceIdToEdit')
    {
      this.router.navigateByUrl('/service/edit/' + data.row.serviceIdToEdit);
    }
    else if(data.column.name=='workflowId')
    {
      this.router.navigateByUrl('/workflow/' + data.row.workflowId);
    }    
    else if(data.column.name=='triggerWorkflowId')
    {
      this.triggerWorkflow(data.row.triggerWorkflowId);
    }
  }
  taskDetails(id: string, taskId: string, allocationId: string, isAdmin:boolean) {
    this.router.navigateByUrl('/task/' + id + '/' + taskId + '/' + allocationId+'/'+isAdmin);
  }

  getTaskStatusClass(status: string) {
    switch (status) {
      case 'Yet to start':
        return 'badge-secondary';
      case 'In-progress':
        return 'badge-warning';
      case 'In-progress':
        return 'badge-success';
      default:
        return 'badge-secondary';
    }
  }

  triggerWorkflow(workflowId: string) {
    this.serviceService.triggerServices(workflowId).subscribe(
      (response: any) => {
        console.log(response)
        if (response.success == true) {
         this.serviceSuccessMsg = response.message;
        }
      });
  }
  onChange(event) {
    if (event.value) {
      this.workflowSelectShow = true;
      this.serviceService.getWorkFlowsForService(event.value).subscribe(
        (response: any) => {
          console.log(response)
          this.workflows = response.data.workflows;
        });
    }
  }
}

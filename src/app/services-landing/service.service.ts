import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  env:any = environment;
  constructor(
    private httpClient: HttpClient
  ) {
  }

  getServices() {
    const url = new URL(this.env.getAllServices);
    return this.httpClient.get(url.toString());
  }
  triggerServices(id) {
    return this.httpClient.get('http://localhost:44339/api/workflow/' + id + '/trigger');
  }
  getWorkFlowsForService(id) {
    return this.httpClient.get('http://localhost:44339/api/service/' + id + '/workflows');
  }

  getServiceById(id) {
    return this.httpClient.get('http://localhost:44339/api/service/' + id);
  }

}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { MustMatch } from '../_helper/must-match.validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements OnInit {
  submitted = false;
  emailOtpSubmitted = false;
  mobileOtpSubmitted = false;
  passwordSubmitted = false;
  regBasicDetails: FormGroup;
  regOtpVerification: FormGroup;
  regPwdSet: FormGroup;
  public showBackLogin;
  public isLinear = true;
  public enableSetPwd: boolean = false;
  public isEmailVerified: boolean = false;
  public isMobVerified: boolean = false;
  public otpRequested: boolean = false;
  public emailOtpRequested: boolean = false;
  public mobOtpRequested: boolean = false;
  public showLoader: boolean = false;
  public RegErrOccured: boolean = false;
  public RegOtpErrOccured: boolean = false;
  public RegPwdErrOccured: boolean = false;
  public showLoaderForEmail: boolean = false;
  public showLoaderForMob: boolean = false;
  public userRegistered: boolean = false;
  public userPwdSet: boolean = false;
  public RegErrMsg: any;
  public RegOtpErrMsg: any;
  public RegPwdErrMsg: any;
  headers: string[];
  showloader: boolean = true;
  router: any;


  constructor(private _formBuilder: FormBuilder, private httpClient: HttpClient,
    private config: ConfigService) { }

  ngOnInit(): void {
    this.regBasicDetails = this._formBuilder.group({
      regtitle: ['', Validators.required],
      regfname: ['', Validators.required],
      regmname: [''],
      reglname: ['', Validators.required],
      regemail: ['', Validators.compose([Validators.required, Validators.email])],
      regmobile: ['', [Validators.required, Validators.minLength(10)]],
    });
    this.regOtpVerification = this._formBuilder.group({
      emailotp: ['', [Validators.required, Validators.minLength(8)]],
      mobotp: ['', [Validators.required, Validators.minLength(8)]]
    });
    this.regPwdSet = this._formBuilder.group({
      regpwd: ['', [Validators.required, Validators.minLength(6)]],
      regconfirmpwd: ['', Validators.required]
    }, {
      validator: MustMatch('regpwd', 'regconfirmpwd')
    });
    localStorage.clear();
    this.showloader = false;
  }

  get registerForm() { return this.regBasicDetails.controls; }
  get otpVerificationForm() { return this.regOtpVerification.controls; }
  get passwordSetForm() { return this.regPwdSet.controls; }
  userRegBasicDetails(stepper: MatStepper) {
    this.submitted = true;
    // stop here if form is invalid
    if (this.regBasicDetails.invalid) {
      return;
    }
    this.otpRequested = true;
    this.showLoader = true;
    this.config.getuserRegister(this.regBasicDetails.value).subscribe(
      (response: any) => {
        if (response.success === true) {
          this.otpRequested = false;
          this.showLoader = false;
          stepper.next();
          sessionStorage.setItem('regEmail', this.regBasicDetails.value.regemail);
          sessionStorage.setItem('regPhone', this.regBasicDetails.value.regmobile);
          sessionStorage.setItem('regUserId', response.data.userId);
        } else {
          this.RegErrOccured = true;
          this.RegErrMsg = response.message;
          this.otpRequested = false;
          this.showLoader = false;
        }
      },
      (error) => {
        this.RegErrOccured = true;
        this.RegErrMsg = 'Something went wrong';
        this.otpRequested = false;
        this.showLoader = false;
      }
    )
  }

  verifyEmail() {
    this.emailOtpSubmitted = true;
    // stop here if form is invalid
    if (this.regOtpVerification.controls.emailotp.invalid) {
      return;
    }
    this.emailOtpRequested = true;
    this.showLoaderForEmail = true;
    const body = {
      "IsToVerify": true,
      "IsEmail": true,
      "Email": sessionStorage.getItem('regEmail'),
      "Otp": this.regOtpVerification.value.emailotp
    }

    this.config.getOtpVerify(body).subscribe(
      (response: any) => {
        if (response.body.success === true) {
          this.isEmailVerified = true;
          this.showLoaderForEmail = false;
          if (this.isEmailVerified && this.isMobVerified) {
            this.enableSetPwd = true;
            let headerToken = response.headers.get('token');
            sessionStorage.setItem('regToken', headerToken);
          }
        } else {
          this.RegOtpErrOccured = true;
          this.RegOtpErrMsg = response.body.message ? response.body.message : 'Please try again';
          this.emailOtpRequested = false;
          this.showLoaderForEmail = false;
        }

      },
      (error) => {
        this.RegOtpErrOccured = true;
        this.RegOtpErrMsg = error.error.message ? error.error.message : 'Please try again';
        this.emailOtpRequested = false;
        this.showLoaderForEmail = false;
      }
    )
  }

  verifyMobile() {
    this.mobileOtpSubmitted = true;
    // stop here if form is invalid
    if (this.regOtpVerification.controls.mobotp.invalid) {
      return;
    }
    this.mobOtpRequested = true;
    this.showLoaderForMob = true;

    const body = {
      "IsToVerify": true,
      "IsEmail": false,
      "Phone": sessionStorage.getItem('regPhone'),
      "Otp": this.regOtpVerification.value.mobotp
    }

    this.config.getOtpVerify(body).subscribe(
      (response: any) => {
        if (response.body.success === true) {
          this.isMobVerified = true;
          this.showLoaderForMob = false;
          if (this.isEmailVerified && this.isMobVerified) {
            this.enableSetPwd = true;
            let headerToken = response.headers.get('token');
            sessionStorage.setItem('regToken', headerToken);
          }
        } else {
          this.RegOtpErrOccured = true;
          this.RegOtpErrMsg = response.body.message ? response.body.message : 'Please try again';
          this.mobOtpRequested = false;
          this.showLoaderForMob = false;
        }

      },
      (error) => {
        this.RegOtpErrOccured = true;
        this.RegOtpErrMsg = error.error.message ? error.error.message : 'Please try again';
        this.mobOtpRequested = false;
        this.showLoaderForMob = false;
      }
    )
  }
  setRegisterPwd() {
    this.userPwdSet = true; 
    this.passwordSubmitted = true;
    // stop here if form is invalid
    if (this.regPwdSet.invalid) {
      this.userPwdSet=false;
      return;
    }
    this.config.getPwdSet(this.regPwdSet.value.regconfirmpwd).subscribe(
      (response: any) => {
        if (response.success === true) {
          this.userRegistered = true;
          sessionStorage.clear();
          localStorage.clear();
        } else {
          this.RegPwdErrOccured = true;
          this.RegPwdErrMsg = response.message;
        }

      },
      (error) => {
        this.RegPwdErrOccured = true;
        this.RegPwdErrMsg = error.error.message;
        this.userPwdSet = false;
      }
    )
  }
}

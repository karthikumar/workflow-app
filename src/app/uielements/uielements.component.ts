import { Component, Input, forwardRef, HostBinding, OnInit, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TaskService } from '../task/services/task.service';

@Component({
  selector: 'app-uielement',
  templateUrl: './uielements.component.html',
  styleUrls: ['./uielements.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UielementsComponent),
      multi: true
    }
  ]
})
export class UielementsComponent implements ControlValueAccessor {
  
  @Input()
  controlParam:any={};

  @Input('value') _value = null;

  @Output()
  actionSelected:EventEmitter<any> = new EventEmitter<any>();

  onChange: any = () => {};
  onTouched: any = () => {};
  propagateChange:any = ()=> {};

  get value() {
    return this._value;
  }

  set value(val) {
    this._value = val;
    this.propagateChange(val);
    this.onTouched();
  }

  constructor(private taskService:TaskService) {}
  

  registerOnChange(fn:any) {
    this.propagateChange = fn;
  }

  writeValue(value) {
    if (value) {
      this.value = value;
    }
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  } 

  textchanged(event)
  {
    this.value = event.target.value;
  }

  selectChanged(event)
  {
    this.value = event.value;
  }

  dateChanged(event)
  {
    this.value = event.value;
  }

  onActionClick(controlParam)
  {
    this.actionSelected.emit(controlParam);
  }

  onFileChange(event)
  {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      let data:any ={};
      data.Files = event.target.files;
      var postData={"id":15,"data":file};
      this.value = '';
      this.taskService.fileUpload(postData).subscribe((taskResponse: any) => {
        let fileName = taskResponse.data[0].filePath;
        let contentType = taskResponse.data[0].contentType; 
        this.value = `?filename=${fileName}&contentType=${contentType}`;
      });      
     
    }
  }
}




// import { Component, OnInit, Input } from '@angular/core';

// @Component({
//   selector: 'app-uielement',
//   templateUrl: './uielements.component.html',
//   styleUrls: ['./uielements.component.scss']
// })
// export class UielementsComponent implements OnInit {

//   constructor() { }

//   @Input() uielement:any={};

//   ngOnInit(): void {
//   }

// }

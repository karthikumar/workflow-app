import { Component, OnInit, ViewChild, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, Validators } from '@angular/forms';
import { FeesChartDialogComponent } from './fees-chart-dialog/fees-chart-dialog.component';
import { AdminService } from '../adminservice/services/admin.service';
import { ConfigService } from '../config.service';
import { HttpClient } from '@angular/common/http';
import { merge, Observable, of as observableOf } from 'rxjs';
import { catchError, map, switchMap, startWith } from 'rxjs/operators';
import { MatSort } from '@angular/material/sort';
import { environment } from 'src/environments/environment';
import { DashboardService } from '../dashboard/services/dashboard.service';

@Component({
  selector: 'app-fees-chart',
  templateUrl: './fees-chart.component.html',
  styleUrls: ['./fees-chart.component.scss']
})

export class FeesChartComponent implements AfterViewInit {
  displayedColumns: string[] = ['serviceName', 'baseFee', 'baseThreshold', 'step', 'increasePerStep', 'action'];

  feesChartDb: ObtainFeesChartList | null;
  data: feesChartList[] = [];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;
  ErrInSearch = false;
  public ServiceGroupNames;
  public FinancialYears;
  public FeesIncreaseBasis;
  feesSearchForm;
  feesFieldList: any = [
    {
      value: 'serviceName',
      name: 'Service name'
    },
    {
      value: 'baseFee',
      name: 'Base fee'
    },
    {
      value: 'baseThreshold',
      name: 'Base threshold'
    },
    {
      value: 'step',
      name: 'Step'
    },
    {
      value: 'increasePerStep',
      name: 'Inc/Step'
    }
  ];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;


  constructor(private _httpClient: HttpClient, public dialog: MatDialog,
    private adminService: AdminService,
    private _formBuilder: FormBuilder,
    private config: ConfigService, public dashServ: DashboardService) { }

  ngOnInit() {
    this.feesSearchForm = this._formBuilder.group({
      feesSearch: '',
      feesSearchField: ''
    });
    this.getMasterData();
  }
  ngAfterViewInit() {
    this.getFeesList(null, null);
  }
  searchFees() {
    this.ErrInSearch = false;
    if (this.feesSearchForm.invalid) {
      this.ErrInSearch = true;
      return;
    }
    this.getFeesList(this.feesSearchForm.getRawValue().feesSearchField, this.feesSearchForm.getRawValue().feesSearch);
  }
  resetForm() {
    this.ErrInSearch = false;
    this.feesSearchForm.reset();
    this.getFeesList(null, null);
  }
  editFeesChartEntry(id, sid, sname, effectiveDate, basefee, incBase, bthreshold, step, incStep) {
    let feeData = {
      id: id,
      sid: sid,
      sname: sname,
      effectiveDate: effectiveDate,
      basefee: basefee,
      incBase,
      bthreshold: bthreshold,
      step: step,
      incStep: incStep
    }
    this.openDialog(feeData);
  }
  getMasterData() {
    this.adminService.getMasterData().subscribe(
      (response: any) => {
        this.FinancialYears = response.data.filter(x => x && x.listName == "FinancialYears")[0].listValues;
        this.FeesIncreaseBasis = response.data.filter(x => x && x.listName == "FeesIncreaseBasis")[0].listValues;
      },
      (error) => {
      });
    this.dashServ.getServices().subscribe((response: any) => {
      this.ServiceGroupNames = response.data.filter(x => x.isAdministrative === false);
    });
  }

  getFeesList(fname, fval) {
    this.feesChartDb = new ObtainFeesChartList(this._httpClient);
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.feesChartDb!.getFeesChartList(
            this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize, fname, fval);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.resultsLength = data.rowCount;
          return data.data;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the GitHub API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      ).subscribe((data: any) => {
        this.data = data
      });
  }

  openDialog(feeData: any) {
    const dialogRef = this.dialog.open(FeesChartDialogComponent, {
      width: '50%'
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getFeesList(null, null);
    });
    dialogRef.componentInstance.servGrpNames = this.ServiceGroupNames;
    dialogRef.componentInstance.finYears = this.FinancialYears;
    dialogRef.componentInstance.feeData = feeData;
    dialogRef.componentInstance.feeIncBasis = this.FeesIncreaseBasis;
  }
}

export interface feesCharApi {
  data: feesChartList[];
  rowCount: number;
}

export interface feesChartList {
  serviceName: string
  baseFee: string;
  increaseBasis: string;
  baseThreshold: string;
  step: number;
  increasePerStep: number;
}

/** An example database that the data source uses to retrieve data for the table. */
export class ObtainFeesChartList {
  constructor(private _httpClient: HttpClient) { }
  env: any = environment;
  getFeesChartList(sort: string, order: string, page: number, size: number, fname: string, fval: string): Observable<feesCharApi> {
    const href = this.env.getFeesChartUrl;
    const requestUrl = `${href}?currentPage=${page + 1}&pageSize=${size}&searchField=${fname}&searchValue=${fval}`;
    return this._httpClient.get<feesCharApi>(requestUrl);
  }
}
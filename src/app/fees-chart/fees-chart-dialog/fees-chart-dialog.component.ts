import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ConfigService } from 'src/app/config.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { FeesChartComponent } from '../fees-chart.component';

@Component({
  selector: 'app-fees-chart-dialog',
  templateUrl: './fees-chart-dialog.component.html',
  styleUrls: ['./fees-chart-dialog.component.scss']
})
export class FeesChartDialogComponent implements OnInit {
  servGrpNames: any;
  finYears: any;
  feeData: any;
  feeIncBasis: any;
  fieldId: any;
  serviceName: any;
  effectiveDt: any;
  baseFee: any;
  incBase: any;
  baseThreshold: any;
  stepField: any;
  incPerStep: any
  showLoaderSubmit: boolean = false;
  fieldValErr: boolean = false;

  feesChart = new FormGroup({
    fieldId: new FormControl(''),
    serviceName: new FormControl(''),
    effectiveDt: new FormControl(''),
    baseFee: new FormControl(''),
    incBase: new FormControl(''),
    baseThreshold: new FormControl(''),
    stepField: new FormControl(''),
    incPerStep: new FormControl('')
  });
  public feesChartStatus: boolean = false;
  public feesChartStatusFailure: boolean = false;

  constructor(private dialogRef: MatDialogRef<FeesChartComponent>,
    private _formBuilder: FormBuilder,
    private config: ConfigService) { }
  ngOnInit(): void {
    this.feesChart = this._formBuilder.group({
      fieldId: '',
      serviceName: ['', Validators.required],
      effectiveDt: ['', Validators.required],
      baseFee: ['', Validators.required],
      incBase: ['', Validators.required],
      baseThreshold: ['', Validators.required],
      stepField: ['', Validators.required],
      incPerStep: ['', Validators.required]
    });
    if (this.feeData) {
      this.feesChart = new FormGroup({
        fieldId: new FormControl(this.feeData.id),
        serviceName: new FormControl(this.feeData.sid),
        effectiveDt: new FormControl(this.feeData.effectiveDate),
        baseFee: new FormControl(this.feeData.basefee),
        incBase: new FormControl(parseInt(this.feeData.incBase)),
        baseThreshold: new FormControl(this.feeData.bthreshold),
        stepField: new FormControl(this.feeData.step),
        incPerStep: new FormControl(this.feeData.incStep)
      });
    }

    if (!this.servGrpNames || !this.finYears || !this.feeIncBasis) {
      this.feesChartStatusFailure = true;
      this.fieldValErr = true;
    }
    if (this.feeData) {
      this.feesChart.controls['serviceName'].disable();
    }
  }
  setFeesChart() {
    if (this.feesChart.invalid) {
      return;
    }
    this.showLoaderSubmit = true;
    this.feesChartStatusFailure = false;
    let arg = this.feesChart.getRawValue();
    if (!arg.fieldId) {
      this.config.submitFeesChart(arg, this.servGrpNames).subscribe(
        (response: any) => {
          if (response.success === true) {
            this.feesChartStatus = true;
            this.showLoaderSubmit = false;
            setTimeout(() => {
              this.dialogRef.close()
            }, 2000);
          } else {
            this.showLoaderSubmit = false;
            this.feesChartStatusFailure = true;
          }

        },
        (error) => {
          this.showLoaderSubmit = false;
        }
      )
    }
    else {
      this.config.modifyFeesChart(arg, this.servGrpNames).subscribe(
        (response: any) => {
          if (response.success === true) {
            this.feesChartStatus = true;
            this.showLoaderSubmit = false;
            setTimeout(() => {
              this.dialogRef.close()
            }, 2000);
          } else {
            this.feesChartStatus = true;
            this.showLoaderSubmit = false;
            setTimeout(() => {
              this.dialogRef.close()
            }, 2000);
          }

        },
        (error) => {
          this.showLoaderSubmit = false;
        }
      )
    }
  }
}

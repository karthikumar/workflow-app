import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeesChartDialogComponent } from './fees-chart-dialog.component';

describe('FeesChartDialogComponent', () => {
  let component: FeesChartDialogComponent;
  let fixture: ComponentFixture<FeesChartDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeesChartDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeesChartDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

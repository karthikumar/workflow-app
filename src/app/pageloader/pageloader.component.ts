import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pageloader',
  templateUrl: './pageloader.component.html',
  styleUrls: ['./pageloader.component.scss']
})
export class PageloaderComponent implements OnInit {
  @Input() showloader: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}

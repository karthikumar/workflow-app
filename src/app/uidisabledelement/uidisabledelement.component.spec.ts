import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UidisabledelementComponent } from './uidisabledelement.component';

describe('UidisabledelementComponent', () => {
  let component: UidisabledelementComponent;
  let fixture: ComponentFixture<UidisabledelementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UidisabledelementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UidisabledelementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

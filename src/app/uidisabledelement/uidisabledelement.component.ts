import { Component, OnInit, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-uidisabledelement',
  templateUrl: './uidisabledelement.component.html',
  styleUrls: ['./uidisabledelement.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UidisabledelementComponent),
      multi: true
    }
  ]
})
export class UidisabledelementComponent implements ControlValueAccessor,OnInit{

 
  @Input()
  controlParam:any={};

  @Input('value') _value = null;  

  filepath:string = '';

  onChange: any = () => {};
  onTouched: any = () => {};
  propagateChange:any = ()=> {};

  get value() {
    return this._value;
  }

  set value(val) {
    this._value = val;
    this.propagateChange(val);
    this.onTouched();
  }

  constructor() {}

  ngOnInit()
  {
    this.filepath = `http://localhost:44339/api/tasks/download${this.controlParam.value}`
  }
  

  registerOnChange(fn:any) {
    this.propagateChange = fn;
  }

  writeValue(value) {
    if (value) {
      this.value = value;
    }
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  } 

  textchanged(event)
  {
    this.value = event.target.value;
  }

  selectChanged(event)
  {
    this.value = event.value;
  }
}

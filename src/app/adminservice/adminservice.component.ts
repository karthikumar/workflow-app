import { Component, OnInit, ViewChild } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { AdminService } from './services/admin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adminservice',
  templateUrl: './adminservice.component.html',
  styleUrls: ['./adminservice.component.scss']
})
export class AdminserviceComponent implements OnInit {
  @ViewChild('stepperPeriod') stepper: MatStepper;
  public serviceform: FormGroup;
  public periodform: FormGroup;
  public singlePeriod: FormGroup;
  public periodupdateform: FormArray;
  public serviceMsg: any;
  public serviceErrMsg: any;
  public adminServiceRes: any;
  public periodDetails: any;
  public serviceWorkflows: any;
  public showCloneIndex: number = -1;
  public masterData: any;
  public showPeriodCross: boolean = false;
  public serviceGroup: any;
  public submitClicked: boolean;
  public readOnly: boolean = true;
  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private router: Router
  ) {
    this.serviceform = this.fb.group({
      serviceName: ['', Validators.required],
      groupName: ['', Validators.required],
      workflowId: [''],
      isBranchAllocatable: [false],
      isRecurring: [false],
      isNegotiable: [false],
      isRecheckRequired: [false]
    }),
      this.periodupdateform = this.fb.array([]),
      this.periodform = this.fb.group({
        period: this.fb.array([])
      })

    const period = this.fb.group({
      name: ['', Validators.required],
      dueDate: ['', [Validators.required, Validators.pattern('[0-3][0-9]\/[0-3][0-9]')]],
      sequence: ['', Validators.required],
      expiryDate: ['', [Validators.required, Validators.pattern('[0-3][0-9]\/[0-3][0-9]')]],
    })

  }

  public showNewPeriodField: boolean = false;
  public showAdminServForm: boolean = true;
  periodDt1: FormControl;

  ngOnInit(): void {
    this.periodDt1 = new FormControl(new Date())
    this.addPeriod();
    this.getMasterData();
    this.GetAllWorkflows();
  }
  get periodForms() {
    return this.periodform.get('period') as FormArray
  }

  addPeriod() {
    console.log(this.periodForms)
    if (this.periodForms.status == "VALID") {
      const period = this.fb.group({
        name: ['', Validators.required],
        dueDate: ['', [Validators.required, Validators.pattern('[0-3][0-9]\/[0-3][0-9]')]],
        sequence: ['', Validators.required],
        expiryDate: ['', [Validators.required, Validators.pattern('[0-3][0-9]\/[0-3][0-9]')]],
      })

      this.periodForms.push(period);
      if (this.periodForms.value.length > 1) {
        this.showPeriodCross = true;
      }
    }
  }

  deletePeriod(i) {
    if (this.periodForms.value.length !== 1) {
      this.periodForms.removeAt(i);
    }
    if (this.periodForms.value.length == 1) {
      this.showPeriodCross = false;
    }
  }
  showNewPeriod(val) {
    if (val == 0) {
      this.showNewPeriodField = true;
    } else {
      this.showNewPeriodField = false;
    }
  }

  adminFunc() {
    this.showAdminServForm = false;
  }

  saveService() {
    console.log(this.periodForms)
    if (this.periodForms.status == "INVALID") {
      return;
    } else if (this.serviceform.status == "INVALID") {
      this.stepper.selectedIndex = 0;
      return;
    }
    this.submitClicked = true;
    let data = {
      name: this.serviceform.value.serviceName,
      groupName: this.serviceform.value.groupName.value,
      groupNameId: this.serviceform.value.groupName.id,
      isBranchAllocatable: this.serviceform.value.isBranchAllocatable,
      isRecurring: this.serviceform.value.isRecurring,
      isNegotiable: this.serviceform.value.isNegotiable,
      isRecheckRequired: this.serviceform.value.isRecheckRequired,
      addPeriods: this.periodform.value.period,
      workflowId: 0
    }

    if(this.serviceform.value.workflowId > 0)
    {
      data.workflowId = this.serviceform.value.workflowId;
    }
    console.log(data)
    this.adminService.saveAdminService(data).subscribe(
      (response: any) => {
        console.log(response)
        if (response.success == true) {
          this.serviceMsg = response.message;
          this.showAdminServForm = false;
          this.serviceErrMsg = null;
          this.adminServiceRes = response.data;
          this.getPeriodDetails();
          this.GetServiceWorkflows();
        } else {
          this.submitClicked = false;
          this.serviceErrMsg = response.message;
          this.serviceMsg = null;
          this.showAdminServForm = true;
        }
        //this.loading = false;
      },
      (error) => {
        this.submitClicked = false;
        //this.loading = false;
        this.serviceErrMsg = error.error.message;
        this.serviceMsg = null;
        this.showAdminServForm = true;
      });
  }
  getPeriodDetails() {
    this.periodDetails = this.adminServiceRes.periods;
    this.periodDetails.forEach(element => {
      this.periodupdateform.controls.push(this.fb.group({
        name: element.name,
        dueDate: element.dueDate,
        expiryDate: element.expiryDate,
        sequence: element.sequence,
        periodId: element.id,
        cloneName: '',
        workflowId: element.workflowId
      }));
    });
  }
  getMasterData() {
    this.adminService.getMasterData().subscribe(
      (response: any) => {
        console.log(response)
        this.masterData = response;
        this.serviceGroup = this.masterData.data.find(x => x.listName == 'ServiceGroupNames').listValues;
      },
      (error) => {
        //this.loading = false;
      });
  }

  editFn() {
    this.readOnly = false;
  }

  showCloneFn(index) {
    this.showCloneIndex = index;
  }
  applyClone(index) {
    let changedData = this.periodupdateform.controls[index].value;
    let data = {
      workflowId: changedData.workflowId,
      name: changedData.cloneName,
      periodId: changedData.periodId
    }
    this.adminService.cloneWorkFlow(data).subscribe(
      (response: any) => {
        console.log(response)
        if (response.success == true) {
          this.serviceMsg = response.message;
          this.CancelFn();
        } else {
          this.serviceErrMsg = response.message;
          this.serviceMsg = null;
        }
      },
      (error) => {
        //this.loading = false;
        this.serviceErrMsg = error.error.message;
        this.serviceMsg = null;
      });
  }
  CancelFn() {
    this.showCloneIndex = -1;
    this.readOnly = true;
    this.serviceErrMsg = null;
  }
  AddFn(index) {
    let changedData = this.periodupdateform.controls[index].value;
    this.router.navigateByUrl('/workflow/' + this.adminServiceRes.id);
  }
  updateFn(index) {
    let changedData = this.periodupdateform.controls[index].value;
    console.log(changedData)
    let data = {
      name: changedData.name,
      id: changedData.periodId,
      dueDate: changedData.dueDate,
      expiryDate: changedData.expiryDate,
      sequence: Number(changedData.sequence)
    }
    this.adminService.updateAdminPeriodData(data).subscribe(
      (response: any) => {
        console.log(response)
        if (response.success == true) {
          this.serviceMsg = response.message;
          this.CancelFn();
        } else {
          this.serviceErrMsg = response.message;
          this.serviceMsg = null;
        }
      },
      (error) => {
        //this.loading = false;
        this.serviceErrMsg = error.error.message;
        this.serviceMsg = null;
      });
  }
  GetServiceWorkflows() {
    // 
    this.adminService.GetServiceWorkflows(this.adminServiceRes.id).subscribe(
      (response: any) => {
        if (response.success == true) {
          this.serviceWorkflows = response.data.workflows;
        }
      });
  }

  GetAllWorkflows() {
    this.adminService.GetAllWorkflows().subscribe(
      (response: any) => {
        if (response.success == true) {
          this.serviceWorkflows = response.data;
        }
      });
  }

  next() {
    console.log(this.serviceform)
    if (this.serviceform.status == "INVALID") {
      return;
    } else {
      this.stepper.selectedIndex = 1;
    }
  }
}

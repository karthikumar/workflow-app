import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(
    private http: HttpClient
  ) { }

  saveAdminService(data) {
    return this.http.post<any>(`http://localhost:44339/api/service/create`, data);
  }

  GetServiceWorkflows(serviceId) {
    return this.http.get<any>('http://localhost:44339/api/service/' + serviceId + '/workflows');
  }

  GetAllWorkflows() {
    return this.http.get<any>('http://localhost:44339/api/workflow/getworkflows');
  }

  updateAdminPeriodData(data) {
    return this.http.post<any>(`http://localhost:44339/api/service/period/update`, data);
  }

  cloneWorkFlow(data) {
    return this.http.post<any>(`http://localhost:44339/api/workflow/clone`, data);
  }

  getMasterData() {
    return this.http.get<any>('http://localhost:44339/api/admin/masterdata');
  }
  GetAdminServiceData(serviceDataId) {
    return this.http.get<any>('http://localhost:44339/api/service/' + serviceDataId);
  }

  updateServiceData(data) {
    return this.http.post<any>(`http://localhost:44339/api/service/modify`, data);
  }
}

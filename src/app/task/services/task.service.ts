import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  env:any = environment;

  constructor(
    private httpClient: HttpClient
  ) { }

  getTaskById(id: string, isAdministrativeTask:boolean) {
    return this.httpClient.post(this.env.taskElementsUrl,{
      "id": +id,
      "isAdministrative": isAdministrativeTask
    } );
  }
  getCompletedTasksByAllocationId(id: string,isAdminTask:boolean) {
    const taskElementsUrl = new URL(this.env.completedTasksUrl);
    taskElementsUrl.searchParams.set('WorkFlowAllocationId', id);
    return this.httpClient.post<any>(taskElementsUrl.toString(),
    {
      "isAdministrative": isAdminTask,
      "workFlowAllocationId": +id
    });
  }

  saveTaskById(postdata: any, isAdminTask:boolean) {
    const taskElementsUrl = new URL(this.env.submitTaskDataUrl);
    postdata.isAdministrative = isAdminTask;
    return this.httpClient.post(taskElementsUrl.toString(), postdata);
  }
  getCompletedTasksDataByAllocationId(id: string, isAdminTask:boolean) {
    const taskElementsUrl = new URL(this.env.complatedTaskDataUrl);
    return this.httpClient.post<any>(this.env.taskElementsUrl,{
      "isAdministrative": isAdminTask,
      "id": +id
    });
  }

  fileUpload(postdata:any)
  {
    const formData = new FormData();
    formData.append('Files', postdata.data);

    const taskElementsUrl = this.env.fileUploadUrl.replace("{id}",postdata.id);
    return this.httpClient.post<any>(taskElementsUrl.toString(), formData);
  }
}

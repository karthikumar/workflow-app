import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TaskService } from './services/task.service';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { TaskFormRoot } from './models/task.model';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
  showloader: boolean = true;
  id: number;
  taskId: number;
  taskForm: FormGroup= new FormGroup({});
  taskUIModel:any;
  taskUICompleteModelCollection:any=[];
  comapletedTasks: any;
  workFlowAllocationId: number;
  stepDataLoadingCompleted: boolean;
  showSave:boolean = true;
  isAdminTask:boolean = false;

  constructor(private route: ActivatedRoute, private fb: FormBuilder, private taskService: TaskService,private router: Router) {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.taskId = +this.route.snapshot.paramMap.get('taskid');
    this.workFlowAllocationId = +this.route.snapshot.paramMap.get('allocationId');
    this.isAdminTask = this.route.snapshot.paramMap.get('isAdministrative')==='true';
  }
  ngOnInit() {
    this.stepDataLoadingCompleted = false;
    this.taskService.getTaskById(this.id.toString(),this.isAdminTask).subscribe((response: any) => {
       this.showloader = false;
      this.taskForm = this.fb.group(this.getBaseFormGroup(response.data))
      this.taskUIModel = this.getTaskUIModel(response.data, 2);
      console.log(JSON.stringify(this.taskUIModel));
      this.GetCompletedTasks();
      
      
    });
  }

  stepperIndex:number;
  showMat:boolean = false;
  private GetCompletedTasks() {
    this.taskService.getCompletedTasksByAllocationId(this.workFlowAllocationId.toString(),this.isAdminTask).subscribe((response: any) => {
      this.comapletedTasks = response;
      this.stepperIndex = response.data.length;
      for (let i = 0; i < response.data.length; i++) {
        this.taskService.getCompletedTasksDataByAllocationId(response.data[i].id.toString(),this.isAdminTask).subscribe((taskResponse: any) => {
          let completedTaskForm = this.fb.group(this.getBaseFormGroup(taskResponse.data))
          let completedTaskUIModel = this.getTaskUIModel(taskResponse.data, 2);
          this.taskUICompleteModelCollection.push({taskUIModel:completedTaskUIModel,taskForm:completedTaskForm});
          
         // this.stepperIndex++;
          this.showMat = true;
        });
      }

      if(response.data.length==0)
      {
        this.showMat = true;
      }
      
      this.stepDataLoadingCompleted = true;
    });
  }
  

  getTaskUIModel(responseData, numOfColumns) {
    let taskUIModel: any = [];

    let index: number = 0;
    let taskRow = [];

    for (let responseItem of responseData) {
      if (responseItem.members.length == 0 && !responseItem.membersWithValue || (responseItem.membersWithValue&&responseItem.membersWithValue.length==0)) {
        let conrolProperties: any = this.getUIElement(responseItem);
        conrolProperties.isGroup = false;
        if(conrolProperties.isAction)
        {
          this.showSave = false;
        }
        taskRow.push(conrolProperties);
      }
      else
      {
          if(taskRow.length>0)
          {
            taskUIModel.push(JSON.parse(JSON.stringify(taskRow)));
            taskRow = [];
          }
          var tuiModel = {
            isGroup:true,
            id:responseItem.id,
            formControlName: responseItem.id,
            groupName:responseItem.name, 
            isArray: responseItem.isRepeatable,
            fields:[]
          }
          if(responseItem.members &&responseItem.members.length>0)
          {
            tuiModel.fields.push({index:0,data:this.getUIElements(responseItem.members,numOfColumns)})
          }
          else if(responseItem.membersWithValue && responseItem.membersWithValue.length>0)
          {
            for(let ind=0; ind<responseItem.membersWithValue.length; ind++)
            {
              tuiModel.fields.push({index:0,data:this.getUIElements(responseItem.membersWithValue[ind],numOfColumns)})
            }

          }

          
          taskUIModel.push(tuiModel);
      }
      if (index > 0 && ((index + 1) % numOfColumns == 0 || index == (responseData.length - 1))) {
        if (taskRow.length) {
          taskUIModel.push(JSON.parse(JSON.stringify(taskRow)));
          taskRow = [];
        }
      }
      index++;
    }

    if(taskRow.length>0)
    {
      taskUIModel.push(taskRow);
    }
    return taskUIModel;

  }


  getUIElements(data, numOfColumns) {
    let index: number = 0;
    let result: any = [];
    let row: any = [];
    for (let item of data) {
      row.push(this.getUIElement(item));

      if (index > 0 && ((index + 1) % numOfColumns == 0 || index == (data.length - 1))) {
        result.push(JSON.parse(JSON.stringify(row)));
        row = [];
      }

      index++;
    }
    if(row.length>0)
    {
      result.push(JSON.parse(JSON.stringify(row)));
    }

    return result;
  }

  getUIElement(data) {
    let control: any = JSON.parse(JSON.stringify(data));
    control.formControlName = data.id;
    control.element = 'input';
    control.type = 'text';
    return control;
  }



  getBaseFormGroup(responseData): any {

    let form: any = {};
    for (let control of responseData) {
      let members: any[] = control.members;
      if (members.length == 0) {
        this.populateFormMember(form, control);
      }
      else {
        if (control.isRepeatable) {
          this.populateFormArray(form, members, control.id)
        }
        else {
          this.populateFormMembers(form, members)
        }
      }

    }
    return form;

  }

  // getFormArray() {
  // this.productForm.get('selling_points') as FormArray;
  //   return this.productForm.get('selling_points') as FormArray;
  // }

  addTaskGroup(groupElements, groupMembers) {
    let formGroup = this.taskForm.get(groupElements.formControlName.toString()) as FormArray;
    formGroup.push(this.fb.group(this.getDynamicArrayMembers(groupMembers)));

    let elementAdd = groupElements.fields[groupElements.fields.length - 1];
    let copyElement = JSON.parse(JSON.stringify(elementAdd));
    copyElement.index++;
    groupElements.fields.push(copyElement);
  }

  removeTaskGroup(uiElement, index) {
    uiElement.fields = uiElement.fields.filter(t => t.index != index);
  }

  getFormArrayMembers(form, controls): any[] {
    let formArrayMembers: any[] = [this.fb.group(this.getArrayMembers(controls))];
    return formArrayMembers;

  }

  getDynamicArrayMembers(group): any {
    let obj: any = {};
    for (let row of group) {
      for (let item of row) {
        obj[item.formControlName] = ['', this.getValidators(item)];
      }
    }
    return obj;

  }

  getArrayMembers(controls): any {
    let obj: any = {};
    for (let control of controls) {
      obj[control.id] = ['', this.getValidators(control)];
    }
    return obj;

  }

  getFormMembers(controls) {
    let form: any = {};
    for (let control of controls) {
      this.populateFormMember(form, control)
    }
    return form;
  }

  populateFormMembers(form, controls) {
    for (let control of controls) {
      this.populateFormMember(form, control)
    }
  }

  populateFormMember(form, control) {
    form[control.id] = ['', this.getValidators(control)];
  }

  getValidators(control)
  {
    debugger;
    let validationsArray = [];
    for(let validation of control.validations)
    {
      if(validation.name == 'Required')
      {
        validationsArray.push(Validators.required);
      }
      if(validation.name == 'Email')
      {
        validationsArray.push(Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"));
      }
      if(validation.name == 'Pattern')
      {
        validationsArray.push(Validators.pattern(validation.value));
      }
      if(validation.name == 'MaxLength')
      {
        validationsArray.push(Validators.maxLength(validation.value));
      }
      if(validation.name == 'Max')
      {
        validationsArray.push(Validators.max(validation.value));
      }
      if(validation.name == 'MinLength')
      {
        validationsArray.push(Validators.minLength(validation.value));
      }
      if(validation.name == 'Min')
      {
        validationsArray.push(Validators.min(validation.value));
      }
    }
    return validationsArray;
  }

  populateFormArray(form, controls, id) {
    let formArrayId = id;
    form[formArrayId] = this.fb.array(this.getFormArrayMembers(form, controls));
  }

  getKeyValueObjFromForm(formObj):  any
  {
    var results:any = [];
    for (var p in formObj) {
      if( formObj.hasOwnProperty(p) ) {
        results.push({'key':p,'value':formObj[p]});
      } 
    }

    for(let result of results)
    {
      if(Array.isArray(result.value))
      {
        result.values = result.value;
        delete result['value'];
        for(let index:number= 0;index<result.values.length;index++)
        {
          result.values[index] = this.getKeyValueObjFromForm(result.values[index]);
        }
      } 
    }
    return results;
  }

  onSubmit() {
    if (this.taskForm.invalid) {
      this.taskForm.markAllAsTouched();
      return;
    }

    const formObj = JSON.parse(JSON.stringify(this.taskForm.value));    
    let postFormModel = this.getKeyValueObjFromForm(formObj);
    this.showloader = true;
    this.taskService.saveTaskById(
    {
      "taskId": this.taskId,
      "taskAllocationId": this.id,
      "data": postFormModel
    },this.isAdminTask).subscribe((response: any) => { 
      this.showloader = false;
      this.router.navigate(['/dashboard']);
    },error=>{
      this.showloader = false;
    });

    

    if (this.taskForm.valid) {
      // ToDo: call above logic here after validation
    }

  }

  onActionSelected(event)
  {
    this.showloader = true;
    this.taskService.saveTaskById(
      {
        "taskId": this.taskId,
        "taskAllocationId": this.id,
        "data": [{"key":event.formControlName.toString(),"value":"true"}]
      },this.isAdminTask).subscribe((response: any) => { 
        this.showloader = false;
        this.router.navigate(['/dashboard']);
      },error=>{
        this.showloader = false;
      });
      
  
  
    console.log(event);
  }
}

export interface TaskFormRoot {
  success: boolean;
  message?: any;
  data: TaskForm[];
}

interface TaskForm {
  id: number;
  taskId: number;
  name: string;
  description?: any;
  dataType: string;
  validations: Validation[];
  selections: any[];
  members: (Member | Members2 | Members3)[];
  isRepeatable: boolean;
  isAttachment: boolean;
  taskDataTypeId?: number;
  action: string;
  taskActionId: number;
  isAction: boolean;
}

interface Members3 {
  id: number;
  name: string;
  description?: any;
  dataType: string;
  validations: Validation[];
  selections: Selection[];
}

interface Members2 {
  id: number;
  name: string;
  description?: any;
  dataType: string;
  validations: (Validation | Validations2)[];
  selections: Selection[];
}

interface Selection {
  name: string;
  value: string;
}

interface Validations2 {
  name: string;
  value?: string;
}

interface Member {
  id: number;
  name: string;
  description?: any;
  dataType: string;
  validations: Validation[];
  selections: any[];
}

interface Validation {
  name: string;
  value?: any;
}
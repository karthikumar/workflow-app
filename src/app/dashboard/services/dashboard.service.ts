import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  env:any = environment;
  constructor(
    private httpClient: HttpClient
  ) {
  }

  getTask() {
    const tasksUrl = new URL(this.env.tasksUrl);
    return this.httpClient.get(tasksUrl.toString());
  }
  getServices() {
    const url = new URL(this.env.getAllServices);
    return this.httpClient.get(url.toString());
  }
  triggerServices(id) {
    return this.httpClient.get('http://localhost:44339/api/workflow/' + id + '/trigger');
  }
  getWorkFlowsForService(id) {
    return this.httpClient.get('http://localhost:44339/api/service/' + id + '/workflows');
  }

  getServiceById(id) {
    return this.httpClient.get('http://localhost:44339/api/service/' + id);
  }

  getClients() {
    return this.httpClient.get('http://localhost:44339/api/clients');
  }

  getBranchesByClient(clientId) {
    return this.httpClient.get('http://localhost:44339/api/clients/'+ clientId + '/branches');
  }
  
  createQuotation(data)
  {
    return this.httpClient.post('http://localhost:44339/api/quotation/create/',data);    
  }

  
}

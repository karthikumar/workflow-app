export interface TaskRoot {
  success: boolean;
  message?: any;
  data: Task[];
}

interface Task {
  id: number;
  taskId: number;
  workFlowAllocationId: number;
  taskName: string;
  status: string;
  sequenceNumber: number;
  description?: any;
  displayType: string;
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ConfigService } from '../config.service';
import { BankDialogComponent } from '../bank-dialog/bank-dialog.component';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.scss']
})
export class BankComponent implements OnInit {


  displayedColumns: string[] =
    [
      'accountName',
      'bankName',
      'bankBranchName',
      'accountNumber',
      'corporateId',
      'corporateUserId',
      'action'
    ];
  public dataSource;
  public branchId;
  public branchName;
  public bankName;
  public clientId;
  public clientPhn;
  public clientEmail;
  isError: boolean = false;
  isLoadingResults: boolean = true;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private route: ActivatedRoute,
    public dialog: MatDialog,
    private config: ConfigService,
    public router: Router) {
    this.clientId = this.route.snapshot.paramMap.get('cid');
    this.branchId = this.route.snapshot.paramMap.get('branchId');
    console.log(this.clientId, this.branchId)
  }

  ngOnInit(): void {
    this.clientId = this.route.snapshot.paramMap.get('cid');
    this.branchId = this.route.snapshot.paramMap.get('branchId');
    this.getBankList();
    this.getClient();
    this.getBranch();
  }

  openDialog(bankData) {
    const dialogRef = this.dialog.open(BankDialogComponent, {
      width: '60%'
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getBankList();
    });
    dialogRef.componentInstance.branchId = this.branchId;
    dialogRef.componentInstance.clientId = this.clientId;
    dialogRef.componentInstance.clientPhn = this.clientPhn;
    dialogRef.componentInstance.clientEmail = this.clientEmail;
    dialogRef.componentInstance.bankData = bankData;

  }

  getClient() {
    this.config.getClient(this.clientId).subscribe(
      (response: any) => {
        if (response.success === true && response.data) {
          this.clientPhn = response.data.phone;
          this.clientEmail = response.data.email;
        } else {
          // this.isError = true;
        }

      },
      (error) => {
        // this.RegErrOccured = true;
        // this.RegErrMsg = error.error.message;
        // this.otpRequested = false;
        // this.showLoader = false;
      }
    )
  }

  getBranch() {
    this.config.getBranch(this.branchId).subscribe(
      (response: any) => {
        if (response.success === true && response.data) {
          this.branchName = response.data.branchName;
        } else {
          // this.isError = true;
        }

      },
      (error) => {
        // this.RegErrOccured = true;
        // this.RegErrMsg = error.error.message;
        // this.otpRequested = false;
        // this.showLoader = false;
      }
    )
  }

  getBankList() {
    this.isLoadingResults = true;
    this.config.getBankList(this.branchId).subscribe(
      (response: any) => {
        if (response.success === true && response.data.length > 0) {
          this.isLoadingResults = false;
          this.isError = false;
          this.dataSource = new MatTableDataSource(response.data);
        } else {
          this.isError = true;
        }

      },
      (error) => {
        // this.RegErrOccured = true;
        // this.RegErrMsg = error.error.message;
        // this.otpRequested = false;
        // this.showLoader = false;
      }
    )
  }

  modifyBank(id, branchId, bankBranchName, bankName, accountName, accountNumber, customerId, iBankPassword, corporateId, corporateUserId, closureDate) {
    let bankData = {
      "id": id,
      "branchId": branchId,
      "bankBranchName": bankBranchName,
      "bankName": bankName,
      "accountName": accountName,
      "accountNumber": accountNumber,
      "customerId": customerId,
      "iBankPassword": iBankPassword,
      "corporateId": corporateId,
      "corporateUserId": corporateUserId,
      "closureDate": closureDate
    }
    this.openDialog(bankData);
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';

@Component({
  selector: 'app-sampleform',
  templateUrl: './sampleform.component.html',
  styleUrls: ['./sampleform.component.scss']
})
export class SampleformComponent implements OnInit {


  public addrDetails;
  public eduDetails;
  @ViewChild('stepperHor') stepperHor: MatStepper;

  constructor(private _formBuilder: FormBuilder,) { }

  ngOnInit(): void {

    this.addrDetails = this._formBuilder.group({
      ownerType: ['', Validators.required],

    });
    this.eduDetails = this._formBuilder.group({
      schl: ['', Validators.required],

    });
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { ConfigService } from '../config.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-set-password',
  templateUrl: './set-password.component.html',
  styleUrls: ['./set-password.component.scss']
})
export class SetPasswordComponent implements OnInit {
  resetPasswordFormGroup: FormGroup;

  public userRegistered: boolean = false;
  public userPwdSet: boolean = false;
  public RegPwdErrOccured: boolean = false;
  public RegPwdErrMsg: any;
  public loginOTPMsg: any;
  public resetPasswordSuccess: boolean = false;
  public resetPasswordErrOccured: boolean = false;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private config: ConfigService,
    private router: Router
  ) { }


  ngOnInit(): void {
    this.resetPasswordFormGroup = this.formBuilder.group({
      regpwd: ['', Validators.required],
      regconfirmpwd: ['', Validators.required]
    }, { validator: this.passwordConfirming });
  }

  get g() { return this.resetPasswordFormGroup.controls; }

  setRegisterPwd() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.resetPasswordFormGroup.invalid) {
      return;
    }
    this.userPwdSet = true;
    this.config.getPwdSet(this.resetPasswordFormGroup.value.regconfirmpwd).subscribe(
      (response: any) => {
        if (response.success === true) {
          this.userRegistered = true;
          sessionStorage.clear();
          localStorage.clear();
          // this.router.navigate(['/login']);
        } else {
          this.RegPwdErrOccured = true;
          this.RegPwdErrMsg = response.message;
        }

      },
      (error) => {
        this.RegPwdErrOccured = true;
        this.RegPwdErrMsg = error.error.message;
        this.userPwdSet = false;
      }
    )
  }
  passwordConfirming(c: AbstractControl): { invalid: boolean } {
    if (c.get('regconfirmpwd').value !== c.get('regpwd').value) {
      c.get('regconfirmpwd').setErrors({ 'noMatch': true });
      return { invalid: true };
    }
  }
}

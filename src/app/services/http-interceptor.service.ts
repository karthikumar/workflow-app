import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginService } from '../login/services/login.service';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  constructor(private loginService: LoginService) { }
  // We inject a LoginService
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // We retrieve the token, if any
    const token = this.loginService.getToken();
    let newHeaders = req.headers;
    if (token && token != null) {
      // If we have a token, we append it to our new headers
      newHeaders = newHeaders.append('Authorization', 'Bearer ' + token);
    }
    // Finally we have to clone our request with our new headers
    // This is required because HttpRequests are immutable 
    const authReq = req.clone({ headers: newHeaders });
    // Then we return an Observable that will run the request
    // or pass it to the next interceptor if any
    return next.handle(authReq).pipe(
      tap(event => {
        if (event instanceof HttpResponse) {
          this.loginService.setToken(event);
        }
      }, error => {
        // http response status code
        console.error("status code:");
        console.error(error.status);
        console.error(error.message);
      })
    );
  }
}

import { Component, ViewEncapsulation } from '@angular/core';
import { NavigationStart, Router, UrlTree, UrlSegmentGroup, UrlSegment, PRIMARY_OUTLET } from '@angular/router';
import { LoginService } from './login/services/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'WFM App';
  userId;
  clientPage = [
    'cliententity',
    'entitybranch',
    'entitybank'
  ]
  constructor(public router: Router, public loginserv: LoginService) {}

  ngOnInit(): void {
    this.router.events.subscribe(value => {
      if (value instanceof NavigationStart) {
        let urlSlice = value.url.slice(1);
        let urlPath = urlSlice.indexOf('/') > -1? urlSlice.slice(0, urlSlice.indexOf('/')) : urlSlice;
        this.clientPage.indexOf(urlPath) < 0 ? sessionStorage.removeItem('clientVerified') : ''; 
        this.userId = this.loginserv.getUser();
      }
    });
  }
}

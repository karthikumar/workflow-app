import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  env: any = environment;

  constructor(private httpClient: HttpClient) { }

  getuserRegister(arg: any) {
    const body = {
      "Title": arg.regtitle,
      "FirstName": arg.regfname,
      "MiddleName": arg.regmname,
      "LastName": arg.reglname,
      "Email": arg.regemail,
      "Phone": arg.regmobile
    };
    return this.httpClient.post(this.env.registerUrl, body);
  }

  getOtpVerify(body: any) {
    return this.httpClient.post(this.env.otpVerifyUrl, body, { observe: 'response' });
  }

  getConsultants() {

    return this.httpClient.get(this.env.getConsultants);
  }

  getPwdSet(arg: any) {

    // var reqHeader = new HttpHeaders({
    //   'Content-Type': 'application/json',
    //   'Authorization': 'Bearer ' + sessionStorage.getItem('regToken')
    // });
    const body = {
      "userId": JSON.parse(sessionStorage.getItem('regUserId')),
      "password": arg
    }
    return this.httpClient.post(this.env.pwdSetUrl, body); //, { headers: reqHeader }
  }

  submitFeesChart(arg, servGrpNames) {
    let bValue = {
      "serviceId": arg.serviceName,
      "serviceName": servGrpNames.filter(x => (x.id === arg.serviceName))[0].value,
      "effectiveDate": arg.effectiveDt,
      "baseFee": arg.baseFee,
      "increaseBasis": (arg.incBase).toString(),
      "baseThreshold": arg.baseThreshold,
      "step": arg.stepField,
      "increasePerStep": arg.incPerStep
    }
    return this.httpClient.post(this.env.feesChartUrl, bValue);
  }

  modifyFeesChart(arg, servGrpNames) {
    let bValue = {
      "id": arg.fieldId,
      "serviceId": arg.serviceName,
      "serviceName": servGrpNames.filter(x => (x.id === arg.serviceName))[0].value,
      "effectiveDate": arg.effectiveDt,
      "baseFee": arg.baseFee,
      "increaseBasis":(arg.incBase).toString(),
      "baseThreshold": arg.baseThreshold,
      "step": arg.stepField,
      "increasePerStep": arg.incPerStep
    }
    return this.httpClient.post(this.env.modifyFeesChartUrl, bValue);
  }

  getClientRegister(arg: any) {
    const body = {
      "title": arg.ctitle,
      "firstName": arg.cname,
      "middleName": arg.cmname,
      "lastName": arg.clname,
      "email": arg.cemail,
      "phone": (arg.cmobile).toString()
    };
    return this.httpClient.post(this.env.getClientRegUrl, body);
  }
  modifyClient(arg: any, id) {
    const body = {
      "id": id,
      "title": arg.ctitle,
      "firstName": arg.cname,
      "middleName": arg.cmname,
      "lastName": arg.clname,
      "email": arg.cemail,
      "phone": (arg.cmobile).toString()
    };
    return this.httpClient.post(this.env.modifyClientUrl, body);
  }
  getClient(cid: any) {
    return this.httpClient.get(this.env.getClientUrl + '/' + cid);
  }
  getEntity(entId: any) {
    return this.httpClient.get(this.env.getEntityUrl + '/' + entId);
  }
  getBranch(branchId: any) {
    return this.httpClient.get(this.env.getBranchUrl + '/' + branchId);
  }
  getClientEntitiesList(cid: any) {
    return this.httpClient.get(this.env.getClientEntitiesListUrl + "?clientId=" + cid);
  }
  deactivateEntity(cid: any) {
    return this.httpClient.get(this.env.deactivateEntity + "?id=" + cid);
  }
  activateEntity(cid: any) {
    return this.httpClient.get(this.env.activateEntity + "?id=" + cid);
  }
  deleteEnt(cid: any) {
    return this.httpClient.get("http://localhost:44339/api/entities/branch/" + cid + "/delete");
  }
  getEntitiesBranchList(cid: any) {
    return this.httpClient.get(this.env.getEntitiesBranchListUrl + "?entityId=" + cid);
  }
  getBankList(entId: any) {
    return this.httpClient.get(this.env.getBankListUrl + "?branchId=" + entId);
  }


  submitClientEntity(id, arg) {
    let bValue = {
      "clientId": parseInt(id),
      "entityName": arg.cliEntName,
      "entityType": arg.cliEntType,
      "pan": arg.cliEntPan,
      "iecCode": arg.cliEntCode,
      "constitution": arg.cliEntConstituition,
      "llP_CIN": arg.cliEntLlc,
      "dob": (arg.cliEntType === 'Individual' && arg.cliEntDob) ? JSON.stringify(arg.cliEntDob).substring(1, 11) : '',
      "aadharNumber": arg.cliEntType === 'Individual' ? arg.cliEntAadhar : '',
      "udyogAadhaar": arg.cliEntType === 'Individual' ? arg.cliEntUAadhar : '',
      "natureofBusiness": arg.cliEntBusiness,
      "efilingPassword": arg.cliEntPwd
    }
    return this.httpClient.post(this.env.getClientEntityUrl, bValue);
  }

  modifyClientEntity(id, clientId, arg) {
    let bValue = {
      "id": parseInt(id),
      "clientId": parseInt(clientId),
      "entityName": arg.cliEntName,
      "entityType": arg.cliEntType,
      "pan": arg.cliEntPan,
      "iecCode": arg.cliEntCode,
      "constitution": arg.cliEntConstituition,
      "llP_CIN": arg.cliEntLlc,
      "dob": (arg.cliEntType === 'Individual' && arg.cliEntDob) ? JSON.stringify(arg.cliEntDob).substring(1, 11) : '',
      "aadharNumber": arg.cliEntType === 'Individual' ? arg.cliEntAadhar : '',
      "udyogAadhaar": arg.cliEntType === 'Individual' ? arg.cliEntUAadhar : '',
      "natureofBusiness": arg.cliEntBusiness,
      "efilingPassword": arg.cliEntPwd
    }
    return this.httpClient.post(this.env.modifyClientEntityUrl, bValue);
  }

  submitEntityBranch(entId, arg) {
    let bValue = {
      "entityId": parseInt(entId),
      "branchName": arg.entBranchName,
      "tanNumber": arg.entTanNum,
      "tanTracesId": arg.entTanTracesId,
      "tdsUserIdTrace": arg.entTdsUserId,
      "tanEfilingId": arg.entTanEfillingId,
      "tdsUserIdTax": arg.entTdsUserIdTax,
      "tanEfilingPassword": arg.entTanEfillingPwd,
      "ptrcNumber": arg.entPtrcNum,
      "ptrcId": arg.entPtrcId,
      "gstNumber": arg.entGstNum,
      "gstLoginId": arg.branchGstLoginId,
      "gstPassword": arg.branchGstPwd,
      "gstEwayBillId": arg.branchEwayBillId,
      "gstEwayBillPassword": arg.branchGstEwayBillPwd,
      "gstEmail": arg.branchGstEmail,
      "gstMobile": arg.branchGstMob,
      "address1": arg.branchAddrOne,
      "address2": arg.branchAddrTwo,
      "locality": arg.branchLocality,
      "city": arg.branchCity,
      "state": arg.branchState,
      "country": arg.branchCountry,
      "zip": arg.branchZip
    }
    return this.httpClient.post(this.env.getEntityBranchUrl, bValue);
  }

  modifyEntityBranch(id, entityId, arg) {
    let bValue = {
      "id": parseInt(id),
      "entityId": parseInt(entityId),
      "branchName": arg.entBranchName,
      "tanNumber": arg.entTanNum,
      "tanTracesId": arg.entTanTracesId,
      "tdsUserIdTrace": arg.entTdsUserId,
      "tanEfilingId": arg.entTanEfillingId,
      "tdsUserIdTax": arg.entTdsUserIdTax,
      "tanEfilingPassword": arg.entTanEfillingPwd,
      "ptrcNumber": arg.entPtrcNum,
      "ptrcId": arg.entPtrcId,
      "gstNumber": arg.entGstNum,
      "gstLoginId": arg.branchGstLoginId,
      "gstPassword": arg.branchGstPwd,
      "gstEwayBillId": arg.branchEwayBillId,
      "gstEwayBillPassword": arg.branchGstEwayBillPwd,
      "gstEmail": arg.branchGstEmail,
      "gstMobile": arg.branchGstMob,
      "address1": arg.branchAddrOne,
      "address2": arg.branchAddrTwo,
      "locality": arg.branchLocality,
      "city": arg.branchCity,
      "state": arg.branchState,
      "country": arg.branchCountry,
      "zip": arg.branchZip
    }
    return this.httpClient.post(this.env.modifyEntityBranchUrl, bValue);
  }


  submitBankData(branchId, arg) {
    let bValue = {
      "branchId": parseInt(branchId),
      "bankBranchName": arg.bankBranchName,
      "bankName": arg.bankName,
      "accountName": arg.accountName,
      "accountNumber": arg.accountNumber,
      "customerId": arg.customerId,
      "iBankPassword": arg.iBankPassword,
      "corporateId": arg.corporateId,
      "corporateUserId": arg.corporateUserId,
      "closureDate": JSON.stringify(arg.closureDate).substring(1, 11)
    }
    return this.httpClient.post(this.env.submitBankUrl, bValue);
  }

  modifyBankData(id, branchId, arg) {
    let bValue = {
      "id": parseInt(id),
      "branchId": parseInt(branchId),
      "bankBranchName": arg.bankBranchName,
      "bankName": arg.bankName,
      "accountName": arg.accountName,
      "accountNumber": arg.accountNumber,
      "customerId": arg.customerId,
      "iBankPassword": arg.iBankPassword,
      "corporateId": arg.corporateId,
      "corporateUserId": arg.corporateUserId,
      "closureDate": JSON.stringify(arg.closureDate).substring(1, 11)
    }
    return this.httpClient.post(this.env.modifyBankUrl, bValue);
  }



}

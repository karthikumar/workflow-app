import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { AdminService } from '../adminservice/services/admin.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatStepper } from '@angular/material/stepper';

@Component({
  selector: 'app-admin-service-edit',
  templateUrl: './admin-service-edit.component.html',
  styleUrls: ['./admin-service-edit.component.scss']
})
export class AdminServiceEditComponent implements OnInit {
  @ViewChild('stepperPeriod') stepper: MatStepper;
  public serviceform: FormGroup;
  public periodform: FormGroup;
  public singlePeriod: FormGroup;
  public periodupdateform: FormArray;
  public serviceMsg: any;
  public serviceErrMsg: any;
  public adminServiceRes: any;
  public periodDetails: any;
  public serviceWorkflows: any;
  public showCloneIndex: number = -1;
  public masterData: any;
  public showPeriodCross: boolean = false;
  public serviceGroup: any;
  public submitClicked: boolean;
  public readOnly: boolean = true;
  public adminServiceId: number;
  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.adminServiceId = this.route.snapshot.params['id'];
    this.serviceform = this.fb.group({
      id: [''],
      serviceName: ['', Validators.required],
      groupName: ['', Validators.required],
      groupNameId: [''],
      isBranchAllocatable: [false],
      isRecurring: [false],
      isNegotiable: [false],
      isRecheckRequired: [false]
    }),
      this.periodupdateform = this.fb.array([]),
      this.periodform = this.fb.group({
        period: this.fb.array([])
      })

    const period = this.fb.group({
      id: [''],
      name: ['', Validators.required],
      dueDate: ['', [Validators.required, Validators.pattern('[0-3][0-9]\/[0-3][0-9]')]],
      sequence: ['', Validators.required],
      expiryDate: ['', [Validators.required, Validators.pattern('[0-3][0-9]\/[0-3][0-9]')]],
    })
  }
  public showNewPeriodField: boolean = false;
  public showAdminServForm: boolean = true;
  periodDt1: FormControl;
  ngOnInit(): void {
    this.periodDt1 = new FormControl(new Date())
    //this.addPeriod();
    this.getMasterData();
    this.getAdminServiceData();
  }
  get periodForms() {
    return this.periodform.get('period') as FormArray
  }
  addPeriod() {
    console.log(this.periodForms)
    if (this.periodForms.status == "VALID") {
      const period = this.fb.group({
        name: ['', Validators.required],
        dueDate: ['', [Validators.required, Validators.pattern('[0-3][0-9]\/[0-3][0-9]')]],
        sequence: ['', Validators.required],
        expiryDate: ['', [Validators.required, Validators.pattern('[0-3][0-9]\/[0-3][0-9]')]],
      })

      this.periodForms.push(period);
      if (this.periodForms.value.length > 1) {
        this.showPeriodCross = true;
      }
    }
  }

  deletePeriod(i) {
    if (this.periodForms.value.length !== 1) {
      this.periodForms.removeAt(i);
    }
    if (this.periodForms.value.length == 1) {
      this.showPeriodCross = false;
    }
  }
  showNewPeriod(val) {
    if (val == 0) {
      this.showNewPeriodField = true;
    } else {
      this.showNewPeriodField = false;
    }
  }

  adminFunc() {
    this.showAdminServForm = false;
  }
  newPeriods = [];
  modifyPeriods = [];
  removePeriods = [];
  periodModifiedIds = [];
  selectedGroup;
  saveAdminService() {
    console.log(this.periodForms)
    console.log(this.serviceform)
    if (this.periodForms.status == "INVALID") {
      return;
    } else if (this.serviceform.status == "INVALID") {
      this.stepper.selectedIndex = 0;
      return;
    }
    this.submitClicked = true;
    this.newPeriods = [];
    this.modifyPeriods = [];
    this.periodModifiedIds = [];
    this.removePeriods = [];
    this.periodform.value.period.forEach(element => {
      if (!element.id) {
        this.newPeriods.push(element);
      } else {
        this.modifyPeriods.push(element);
        this.periodModifiedIds.push(element.id)
      }
    });
     //let filteredData= this.periodIds.filter(el => this.periodModifiedIds.indexOf(el) < 0);
     this.removePeriods = this.periodIds.filter(el => this.periodModifiedIds.indexOf(el) < 0);
    console.log(this.removePeriods); 
    this.selectedGroup = this.serviceGroup.find(c => c.id == this.serviceform.value.groupName);
    let data = {
      id: this.serviceform.value.id,
      name: this.serviceform.value.serviceName,
      groupName: this.selectedGroup.value,
      groupNameId: this.selectedGroup.id,
      isBranchAllocatable: this.serviceform.value.isBranchAllocatable,
      isRecurring: this.serviceform.value.isRecurring,
      isNegotiable: this.serviceform.value.isNegotiable,
      isRecheckRequired: this.serviceform.value.isRecheckRequired,
      addPeriods: this.newPeriods,
      modifyPeriods: this.modifyPeriods,
      removePeriods: this.removePeriods
    }
    console.log(this.periodform.value.period);
    console.log(this.periodModifiedIds)
    console.log(this.serviceData.periods);
    console.log(this.periodIds)
    
    console.log(data)
    this.adminService.updateServiceData(data).subscribe(
      (response: any) => {
        console.log(response)
        if (response.success == true) {
          this.serviceMsg = response.message;
          this.showAdminServForm = false;
          this.serviceErrMsg = null;
          this.adminServiceRes = response.data;          
    this.router.navigateByUrl('/service/' + this.adminServiceRes.id);
          // this.getPeriodDetails();
          // this.GetServiceWorkflows();
        } else {
          this.submitClicked = false;
          this.serviceErrMsg = response.message;
          this.serviceMsg = null;
          this.showAdminServForm = true;
        }
        //this.loading = false;
      },
      (error) => {
        this.submitClicked = false;
        //this.loading = false;
        this.serviceErrMsg = error.error.message;
        this.serviceMsg = null;
        this.showAdminServForm = true;
      });
  }
  getPeriodDetails() {
    this.periodDetails = this.adminServiceRes.periods;
    this.periodDetails.forEach(element => {
      this.periodupdateform.controls.push(this.fb.group({
        name: element.name,
        dueDate: element.dueDate,
        expiryDate: element.expiryDate,
        sequence: element.sequence,
        periodId: element.id,
        cloneName: '',
        workflowId: element.workflowId
      }));
    });
  }
  getMasterData() {
    this.adminService.getMasterData().subscribe(
      (response: any) => {
        console.log(response)
        this.masterData = response;
        this.serviceGroup = this.masterData.data.find(x => x.listName == 'ServiceGroupNames').listValues;
      },
      (error) => {
        //this.loading = false;
      });
  }

  editFn() {
    this.readOnly = false;
  }

  showCloneFn(index) {
    this.showCloneIndex = index;
  }
  applyClone(index) {
    let changedData = this.periodupdateform.controls[index].value;
    let data = {
      workflowId: changedData.workflowId,
      name: changedData.cloneName,
      periodId: changedData.periodId
    }
    this.adminService.cloneWorkFlow(data).subscribe(
      (response: any) => {
        console.log(response)
        if (response.success == true) {
          this.serviceMsg = response.message;
          this.CancelFn();
        } else {
          this.serviceErrMsg = response.message;
          this.serviceMsg = null;
        }
      },
      (error) => {
        //this.loading = false;
        this.serviceErrMsg = error.error.message;
        this.serviceMsg = null;
      });
  }
  CancelFn() {
    this.showCloneIndex = -1;
    this.readOnly = true;
    this.serviceErrMsg = null;
  }
  AddFn(index) {
    let changedData = this.periodupdateform.controls[index].value;
    this.router.navigateByUrl('/workflow/' + this.adminServiceRes.id);
  }
  updateFn(index) {
    let changedData = this.periodupdateform.controls[index].value;
    console.log(changedData)
    let data = {
      name: changedData.name,
      id: changedData.periodId,
      dueDate: changedData.dueDate,
      expiryDate: changedData.expiryDate,
      sequence: Number(changedData.sequence)
    }
    this.adminService.updateAdminPeriodData(data).subscribe(
      (response: any) => {
        console.log(response)
        if (response.success == true) {
          this.serviceMsg = response.message;
          this.CancelFn();
        } else {
          this.serviceErrMsg = response.message;
          this.serviceMsg = null;
        }
      },
      (error) => {
        //this.loading = false;
        this.serviceErrMsg = error.error.message;
        this.serviceMsg = null;
      });
  }
  GetServiceWorkflows() {
    // 
    this.adminService.GetServiceWorkflows(this.adminServiceRes.id).subscribe(
      (response: any) => {
        if (response.success == true) {
          this.serviceWorkflows = response.data.workflows;
        }
      });
  }

  next() {
    console.log(this.serviceform)
    if (this.serviceform.status == "INVALID") {
      return;
    } else {
      this.stepper.selectedIndex = 1;
    }
  }
  serviceData;
  periodIds: any = [];
  getAdminServiceData() {
    this.adminService.GetAdminServiceData(this.adminServiceId).subscribe(
      (response: any) => {
        if (response.success == true) {
          this.serviceData = response.data;
          this.serviceform.controls.id.setValue(this.serviceData.id);
          this.serviceform.controls.serviceName.setValue(this.serviceData.name);
          this.serviceform.controls.groupName.setValue(this.serviceData.groupNameId);
          this.serviceform.controls.isBranchAllocatable.setValue(this.serviceData.isBranchAllocatable);
          this.serviceform.controls.isRecheckRequired.setValue(this.serviceData.isRecheckRequired);
          this.serviceform.controls.isRecurring.setValue(this.serviceData.isRecurring);
          this.serviceform.controls.isNegotiable.setValue(this.serviceData.isNegotiable);
          //this.getPeriodDetails();
          this.periodform.controls.period['controls'].pop(0);
          this.periodIds = [];
          this.serviceData.periods.forEach(element => {
            if (element.id) {
              this.periodIds.push(element.id);
            }
            let single = new FormGroup({
              id: new FormControl(element.id),
              name: new FormControl(element.name, Validators.required),
              dueDate: new FormControl(element.dueDate, [Validators.required, Validators.pattern('[0-3][0-9]\/[0-3][0-9]')]),
              expiryDate: new FormControl(element.expiryDate,  [Validators.required, Validators.pattern('[0-3][0-9]\/[0-3][0-9]')]),
              sequence: new FormControl(element.sequence, Validators.required)
            });
            
      this.periodForms.push(single);
            // this.periodform.controls.period['controls'].push(single);
          });
          if (this.serviceData.periods.length > 1) {
            this.showPeriodCross = true;
          }
        }
      });
  }
}
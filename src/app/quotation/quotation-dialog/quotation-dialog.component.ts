import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DashboardService } from 'src/app/dashboard/services/dashboard.service';
import { AdminService } from 'src/app/adminservice/services/admin.service';
import { analyzeAndValidateNgModules, ConditionalExpr } from '@angular/compiler';
import { ConfigService } from 'src/app/config.service';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-quotation-dialog',
  templateUrl: './quotation-dialog.component.html',
  styleUrls: ['./quotation-dialog.component.scss']
})
export class QuotationDialogComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private dashboardService:DashboardService,
    private adminService:AdminService,
    private configService:ConfigService,
    private dialogRef: MatDialogRef<QuotationDialogComponent>
    ) { }

  quotation:FormGroup;
  financialYears:any;
  consultants:any;
  clients:any;
  services:any;
  periods:any={};
  branches:any={};
  showLoaderSubmit: boolean = false;
  quotationStatus: boolean = false;
  RegOtpErrMsg;
  quotationStatusFailure: boolean = false;
  subscriptionValid: boolean = false;
  
  position:number=0;
  ngOnInit(): void {
    this.quotation = this.formBuilder.group({
      name: ['', Validators.required],
      consultantId: ['', Validators.required],
      isGSTApplicable: ['', Validators.required],
      gstRate: ['', Validators.required],
      isTDSApplicable:['', Validators.required],
      tdsSection: ['', Validators.required],
      tdsPercentage:['', Validators.required],
      tdsAmount:['', Validators.required],
      addSubscriptions: this.formBuilder.array([this.getFormArrayMembers()]),
  
    });

    

    this.adminService.getMasterData().subscribe(
      (response: any) => {
        this.financialYears = response.data.filter(x => x && x.listName == "FinancialYears")[0].listValues;
        
      },
      (error) => {
      });

    this.dashboardService.getServices().subscribe((response: any) => {
      this.services = response.data.filter(x => x.isAdministrative === false);
    });

    this.dashboardService.getClients().subscribe((response: any) => {
        this.clients = response.data;
    });

    

    this.configService.getConsultants().subscribe((response: any) => {
    this.consultants = response.data;
    });

  }

  onServiceChange(event,idx)
  {
    this.dashboardService.getServiceById(event.value).subscribe((response:any)=>{
      this.periods[idx] = response.data.periods;
    });
  }

  onClientChange(event,idx)
  {
    this.dashboardService.getBranchesByClient(event.value).subscribe((response:any)=>{
         this.branches[idx] = response.data;
    });
  }

  

  createQuotation(data)
  {
    if (this.quotation.invalid) {
      return;
    }
    this.showLoaderSubmit = true;
    this.dashboardService.createQuotation(data).subscribe((response:any)=>{
      console.log(response);
      if (response.success === true) {
        this.quotationStatus = true;
        this.showLoaderSubmit = false;
        setTimeout(() => {
          this.dialogRef.close()
        }, 2000);
      } else {
        this.showLoaderSubmit = false;
        this.RegOtpErrMsg = response.message;
        this.quotationStatusFailure = true;
      }
    })
  }

  // getclientList(sort: string, order: string, page: number, size: number): Observable<clientListApi> {
  //   const href = "http://localhost:44339/api/clients/getclients";
  //   const requestUrl = `${href}?currentPage=${page + 1}&pageSize=${size}`;
  //   return this._httpClient.get<clientListApi>(requestUrl);
  // }

  getFormArrayMembers(): any {
    let formArrayMembers: any = this.formBuilder.group(
      {
        serviceId: ['', Validators.required],
        branchId: ['', Validators.required],
        financialYearId: ['', Validators.required],
        qClients: ['', Validators.required],
        periods:['', Validators.required],
        amount1: ['', Validators.required],
        amount2: ['', Validators.required],
        amount3: ['', Validators.required],
        amount4: ['', Validators.required],
        advanceAmount: ['', Validators.required],
        name: ['', Validators.required]
        
      }
      );
    return formArrayMembers;

  }

  get subscriptionsArray(): FormArray{
	  return this.quotation.get('addSubscriptions') as FormArray;
  }
  addSubscriptions(){
    this.subscriptionValid = false;
    if (!this.quotation.get('addSubscriptions').valid) {
      this.subscriptionValid = true;
      return;
    }
    let fg = this.getFormArrayMembers();
    this.subscriptionsArray.push(fg);	  
   
  }
  deleteSubscription(idx: number, val) {
    this.subscriptionsArray.removeAt(idx);
    
  }

}

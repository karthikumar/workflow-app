import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { QuotationDialogComponent } from './quotation-dialog/quotation-dialog.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { HttpClient } from '@angular/common/http';
import { merge, Observable, of as observableOf } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-quotation',
  templateUrl: './quotation.component.html',
  styleUrls: ['./quotation.component.scss']
})
export class QuotationComponent implements OnInit {
  displayedColumns: string[] = ['name', 'consultantName', 'status', 'createdDate', 'subscriptionCount', 'action'];

  clientListDb: obtainQuotationList | null;
  data: clientList[] = [];
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  constructor(private _httpClient: HttpClient, public dialog: MatDialog) { }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.getQuotationList();
  }

  getQuotationList() {
    this.clientListDb = new obtainQuotationList(this._httpClient);
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.clientListDb!.getQuotationList(
            this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          if(data && data.data.length > 0){
            this.isLoadingResults = false;
            this.isRateLimitReached = false;
            this.resultsLength = data.rowCount;
            return data.data;

          } else {
            this.isLoadingResults = false;
            this.isRateLimitReached = true;
          }
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the GitHub API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      ).subscribe((data: any) => this.data = data);
  }

  openDialog() {
    const dialogRef = this.dialog.open(QuotationDialogComponent, {
      width: '60%'
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getQuotationList();
    });

  }
}

export interface quotationListApi {
  data: clientList[];
  rowCount: number;
}

export interface clientList {
  id: number;
  name: string;
  consultantName: string
  status: string
  subscriptionCount: string
  createdDate: string;
}

/** An example database that the data source uses to retrieve data for the table. */
export class obtainQuotationList {
  constructor(private _httpClient: HttpClient) { }
  env:any = environment;
  getQuotationList(sort: string, order: string, page: number, size: number): Observable<quotationListApi> {
    const href = this.env.getQuotationListUrl;
    const requestUrl = `${href}?currentPage=${page + 1}&pageSize=${size}`;
    return this._httpClient.get<quotationListApi>(requestUrl);
  }
}
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { ConfigService } from 'src/app/config.service';
import { MatDialogRef } from '@angular/material/dialog';
import { ClientListComponent } from '../client-list.component';
import { LoginService } from 'src/app/login/services/login.service';

@Component({
  selector: 'app-client-list-dialog',
  templateUrl: './client-list-dialog.component.html',
  styleUrls: ['./client-list-dialog.component.scss']
})
export class ClientListDialogComponent implements OnInit {

  public clientDetails;
  public verifyClientOtp;
  public verifyClientMobOtp;
  public verifyClientEmailOtp;
  public RegOtpErrMsg;
  public modelType;
  public modelVal;
  public getMobSolo;
  public getOtpToMode;
  public getEmailSolo;
  public verfiyMobSolo;
  public verifyEmailSolo;
  public verifyOtpToMode;
  public cliModifySuccess: boolean = false;
  isLinear = true;
  showLoader: boolean = false;
  RegErrOccured: boolean = false;
  RegErrMsg: boolean = false;
  otpRequested: boolean = false;
  isEmailVerified: boolean = false;
  emailOtpSubmitted: boolean = false;
  showLoaderForEmail: boolean = false;
  RegOtpErrOccured: boolean = false;
  isMobVerified: boolean = false;
  otpVerified: boolean = false;
  otpReceived: boolean = false;
  showLoaderForMobSolo: boolean = false;
  clientEntVerified: boolean;
  showLoaderForMob: boolean = false;


  @ViewChild('stepperHor') stepperHor: MatStepper;

  constructor(private dialogRef: MatDialogRef<ClientListComponent>,
    private _formBuilder: FormBuilder,
    private config: ConfigService, public loginServ: LoginService) { }

  ngOnInit(): void {
    this.clientDetails = this._formBuilder.group({
      ctitle: ['', Validators.required],
      cname: ['', Validators.required],
      cmname: ['', Validators.required],
      clname: ['', Validators.required],
      cmobile: ['', [Validators.required, Validators.pattern(/^[6-9]\d{9}$/)]],
      cemail: ['', [Validators.required, Validators.email]]
    });
    this.verifyClientMobOtp = this._formBuilder.group({
      cmobotp: ['', Validators.required]
    });
    this.verifyOtpToMode = this._formBuilder.group({
      otpfield: ['', Validators.required]
    });
    this.verifyClientEmailOtp = this._formBuilder.group({
      cemailotp: ['', Validators.required],
    });
    this.getMobSolo = this._formBuilder.group({
      cmobsolo: ''
    });
    this.getOtpToMode = this._formBuilder.group({
      isEmail: 'false'
    });
    if (this.getMobSolo) {
      this.getMobSolo = new FormGroup({
        cmobsolo: new FormControl(this.modelVal)
      });
    }
    this.verfiyMobSolo = this._formBuilder.group({
      cmobotpsolo: ''
    });
    this.getMobSolo.controls['cmobsolo'].disable();

    this.getEmailSolo = this._formBuilder.group({
      cemailsolo: ''
    });
    if (this.getEmailSolo) {
      this.getEmailSolo = new FormGroup({
        cemailsolo: new FormControl(this.modelVal)
      });
    }
    this.verifyEmailSolo = this._formBuilder.group({
      cemailotpsolo: ''
    });
    this.getEmailSolo.controls['cemailsolo'].disable();

    if (this.modelType === 'modify') {
      this.clientDetails.get('ctitle').setValue(this.modelVal.title);
      this.clientDetails.get('cname').setValue(this.modelVal.firstName);
      this.clientDetails.get('cmname').setValue(this.modelVal.middleName);
      this.clientDetails.get('clname').setValue(this.modelVal.lastName);
      this.clientDetails.get('cmobile').setValue(this.modelVal.phone);
      this.clientDetails.get('cemail').setValue(this.modelVal.email);
    }
    this.clientEntVerified = sessionStorage.getItem('clientVerified') ? JSON.parse(sessionStorage.getItem('clientVerified')) : false;
  }
  ngAfterViewInit() {
  }

  submitClientOtp(stepper: MatStepper) {
    // stop here if form is invalid
    if (this.clientDetails.invalid) {
      return;
    }
    this.showLoader = true;
    this.RegErrOccured = false;
    if (this.modelVal.id) {
      this.config.modifyClient(this.clientDetails.value, this.modelVal.id).subscribe(
        (response: any) => {
          if (response.success === true) {
            this.showLoader = false;
            this.cliModifySuccess = true;
            sessionStorage.setItem('clientEmail', this.clientDetails.value.cemail);
            sessionStorage.setItem('clientPhone', this.clientDetails.value.cmobile);
            sessionStorage.setItem('clientId', response.data.id);
            setTimeout(() => {
              this.dialogRef.close()
            }, 2000);
          } else {
            this.RegErrOccured = true;
            this.RegErrMsg = response.message;
            this.otpRequested = false;
            this.showLoader = false;
          }
        },
        (error) => {
          this.RegErrOccured = true;
          this.RegErrMsg = error.error.message;
          this.otpRequested = false;
          this.showLoader = false;
        }
      )

    } else {
      this.config.getClientRegister(this.clientDetails.value).subscribe(
        (response: any) => {
          if (response.success === true) {
            this.otpRequested = false;
            this.showLoader = false;
            stepper.next();
            sessionStorage.setItem('clientEmail', this.clientDetails.value.cemail);
            sessionStorage.setItem('clientPhone', this.clientDetails.value.cmobile);
            sessionStorage.setItem('clientId', response.data.id);
          } else {
            this.RegErrOccured = true;
            this.RegErrMsg = response.message;
            this.otpRequested = false;
            this.showLoader = false;
          }

        },
        (error) => {
          this.RegErrOccured = true;
          this.RegErrMsg = error.error.message;
          this.otpRequested = false;
          this.showLoader = false;
        }
      )
    }
  }

  verifyClientEmail() {
    this.emailOtpSubmitted = true;
    // stop here if form is invalid
    if (this.verifyClientEmailOtp.invalid) {
      return;
    }
    this.showLoaderForEmail = true;
    const body = {
      "isToVerifyClient": true,
      "isEmail": true,
      "email": sessionStorage.getItem('clientEmail'),
      "otp": this.verifyClientEmailOtp.value.cemailotp
    }

    this.config.getOtpVerify(body).subscribe(
      (response: any) => {
        if (response.body.success === true) {
          this.isEmailVerified = true;
          this.RegOtpErrOccured = false;
          this.showLoaderForEmail = false;
          if (this.isEmailVerified && this.isMobVerified) {
            this.otpVerified = true;
            sessionStorage.removeItem('clientEmail');
            sessionStorage.removeItem('clientPhone');
            sessionStorage.removeItem('clientId');
            setTimeout(() => {
              this.dialogRef.close()
            }, 2000);
          }
        } else {
          this.RegOtpErrOccured = true;
          this.RegOtpErrMsg = 'Please enter correct OTP and try again';
          this.showLoaderForEmail = false;
        }

      },
      (error) => {
        this.RegOtpErrOccured = true;
        this.RegOtpErrMsg = 'Please enter correct OTP and try again';
        this.showLoaderForEmail = false;
      }
    )
  }

  verifyClientMobile() {
    // stop here if form is invalid
    if (this.verifyClientMobOtp.invalid) {
      return;
    }
    this.showLoaderForMob = true;

    const body = {
      "isToVerifyClient": true,
      "isEmail": false,
      "phone": sessionStorage.getItem('clientPhone'),
      "otp": this.verifyClientMobOtp.value.cmobotp
    }

    this.config.getOtpVerify(body).subscribe(
      (response: any) => {
        if (response.body.success === true) {
          this.isMobVerified = true;
          this.RegOtpErrOccured = false;
          this.showLoaderForMob = false;
          if (this.isEmailVerified && this.isMobVerified) {
            this.otpVerified = true;
            sessionStorage.removeItem('clientEmail');
            sessionStorage.removeItem('clientPhone');
            sessionStorage.removeItem('clientId');
            setTimeout(() => {
              this.dialogRef.close()
            }, 2000);
          }
        } else {
          this.RegOtpErrOccured = true;
          this.RegOtpErrMsg = 'Please enter correct OTP and try again';
          this.showLoaderForMob = false;
        }

      },
      (error) => {
        this.RegOtpErrOccured = true;
        this.RegOtpErrMsg = 'Please enter correct OTP and try again';
        this.showLoaderForMob = false;
      }
    )
  }

  getClientMobileOtp() {
    this.showLoaderForMobSolo = true;

    const body = {
      "isToVerifyClient": true,
      "phone": this.getMobSolo.getRawValue().cmobsolo
    }

    this.loginServ.getOTP(body).subscribe(
      (response: any) => {
        if (response.success === true) {
          this.showLoaderForMobSolo = false;
          this.otpReceived = true;
        } else {
          this.RegOtpErrOccured = true;
          this.RegOtpErrMsg = 'Please try again';
          this.showLoaderForMob = false;
        }

      },
      (error) => {
        this.RegOtpErrOccured = true;
        this.RegOtpErrMsg = 'Please try again';
        this.showLoaderForMob = false;
      }
    )
  }
  getOtpToType() {
    this.showLoaderForMobSolo = true;
    const body = {
      "isToEditClient": true,
      "isEmail": JSON.parse(this.getOtpToMode.getRawValue().isEmail),
      "clientId": this.modelVal.id
    }

    this.loginServ.getOTP(body).subscribe(
      (response: any) => {
        if (response.success === true) {
          this.showLoaderForMobSolo = false;
          this.otpReceived = true;
        } else {
          this.RegOtpErrOccured = true;
          this.RegOtpErrMsg = 'Please try again';
          this.showLoaderForMob = false;
        }

      },
      (error) => {
        this.RegOtpErrOccured = true;
        this.RegOtpErrMsg = 'Please try again';
        this.showLoaderForMob = false;
      }
    )
  }

  verifyOtpToType() {
    // stop here if form is invalid
    this.RegOtpErrOccured = false;
    if (this.verifyOtpToMode.invalid) {
      return;
    }
    this.showLoaderForMob = true;

    const body = {
      "isToEditClient": true,
      "isEmail": JSON.parse(this.getOtpToMode.getRawValue().isEmail),
      "otp": this.verifyOtpToMode.value.otpfield,
      "clientId": this.modelVal.id
    }

    this.config.getOtpVerify(body).subscribe(
      (response: any) => {
        if (response.body.success === true) {
          this.isMobVerified = true;
          this.RegOtpErrOccured = false;
          this.showLoaderForMob = false;
          this.otpVerified = true;
          setTimeout(() => {
            this.clientEntVerified = true;
          }, 2000);
        } else {
          this.RegOtpErrOccured = true;
          this.RegOtpErrMsg = 'Please enter correct OTP and try again';
          this.showLoaderForMob = false;
        }

      },
      (error) => {
        this.RegOtpErrOccured = true;
        this.RegOtpErrMsg = 'Please enter correct OTP and try again';
        this.showLoaderForMob = false;
      }
    )
  }

  verifyClientMobileSolo() {
    // stop here if form is invalid
    if (this.verfiyMobSolo.invalid) {
      return;
    }
    this.showLoaderForMob = true;

    const body = {
      "isToVerifyClient": true,
      "isEmail": false,
      "phone": this.modelVal,
      "otp": this.verfiyMobSolo.value.cmobotpsolo
    }

    this.config.getOtpVerify(body).subscribe(
      (response: any) => {
        if (response.body.success === true) {
          this.isMobVerified = true;
          this.RegOtpErrOccured = false;
          this.showLoaderForMob = false;
          this.otpVerified = true;
          setTimeout(() => {
            this.dialogRef.close()
          }, 2000);
        } else {
          this.RegOtpErrOccured = true;
          this.RegOtpErrMsg = 'Please enter correct OTP and try again';
          this.showLoaderForMob = false;
        }

      },
      (error) => {
        this.RegOtpErrOccured = true;
        this.RegOtpErrMsg = 'Please enter correct OTP and try again';
        this.showLoaderForMob = false;
      }
    )
  }

  getClientEmailOtp() {
    this.showLoaderForMobSolo = true;

    const body = {
      "isToVerifyClient": true,
      "isEmail": true,
      "email": this.getEmailSolo.getRawValue().cemailsolo
    }

    this.loginServ.getOTP(body).subscribe(
      (response: any) => {
        if (response.success === true) {
          this.showLoaderForMobSolo = false;
          this.otpReceived = true;
        } else {
          this.RegOtpErrOccured = true;
          this.RegOtpErrMsg = 'Please try again';
          this.showLoaderForMob = false;
        }

      },
      (error) => {
        this.RegOtpErrOccured = true;
        this.RegOtpErrMsg = 'Please try again';
        this.showLoaderForMob = false;
      }
    )
  }

  verifyClientEmailSolo() {
    // stop here if form is invalid
    if (this.verifyEmailSolo.invalid) {
      return;
    }
    this.showLoaderForMob = true;

    const body = {
      "isToVerifyClient": true,
      "isEmail": true,
      "email": this.modelVal,
      "otp": this.verifyEmailSolo.value.cemailotpsolo
    }

    this.config.getOtpVerify(body).subscribe(
      (response: any) => {
        if (response.body.success === true) {
          this.isMobVerified = true;
          this.RegOtpErrOccured = false;
          this.showLoaderForMob = false;
          this.otpVerified = true;
          setTimeout(() => {
            this.dialogRef.close()
          }, 2000);
        } else {
          this.RegOtpErrOccured = true;
          this.RegOtpErrMsg = 'Please enter correct OTP and try again';
          this.showLoaderForMob = false;
        }

      },
      (error) => {
        this.RegOtpErrOccured = true;
        this.RegOtpErrMsg = 'Please enter correct OTP and try again';
        this.showLoaderForMob = false;
      }
    )
  }


}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientListDialogComponent } from './client-list-dialog.component';

describe('ClientListDialogComponent', () => {
  let component: ClientListDialogComponent;
  let fixture: ComponentFixture<ClientListDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientListDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientListDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientEntitiesDialogComponent } from './client-entities-dialog.component';

describe('ClientEntitiesDialogComponent', () => {
  let component: ClientEntitiesDialogComponent;
  let fixture: ComponentFixture<ClientEntitiesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientEntitiesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientEntitiesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

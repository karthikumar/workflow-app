import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ConfigService } from 'src/app/config.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { LoginService } from 'src/app/login/services/login.service';

@Component({
  selector: 'app-client-entities-dialog',
  templateUrl: './client-entities-dialog.component.html',
  styleUrls: ['./client-entities-dialog.component.scss']
})
export class ClientEntitiesDialogComponent implements OnInit {

  public clientId;
  public clientPhn;
  public clientEmail;
  public clientEntityData;
  cliEntityStatus;
  // public getMobSolo;
  // public getEmailSolo;
  // public verfiyMobSolo;
  // public verifyEmailSolo;
  public verifyOtpToMode;
  public getOtpToMode;

  fieldValErr;
  RegOtpErrMsg;
  showLoaderSubmit: boolean = false;
  cliEntyStatusFailure: boolean = false;
  individualField: boolean = false;
  clientEntVerified: boolean;
  otpReceived: boolean = false;
  showLoaderForMobSolo: boolean = false;
  isMobVerified: boolean = false;
  showLoaderForMob: boolean = false;
  RegOtpErrOccured: boolean = false;
  otpVerified: boolean = false;

  clientEntity = new FormGroup({
    cliEntName: new FormControl(''),
    cliEntType: new FormControl(''),
    cliEntDob: new FormControl(''),
    cliEntAadhar: new FormControl(''),
    cliEntUAadhar: new FormControl(''),
    cliEntPan: new FormControl(''),
    cliEntCode: new FormControl(''),
    cliEntConstituition: new FormControl(''),
    cliEntLlc: new FormControl(''),
    cliEntBusiness: new FormControl(''),
    cliEntPwd: new FormControl('')
  });

  constructor(private _formBuilder: FormBuilder,
    private config: ConfigService,
    private loginServ: LoginService,
    private dialogRef: MatDialogRef<ClientEntitiesDialogComponent>) { }

  ngOnInit(): void {
    this.clientEntity = this._formBuilder.group({
      cliEntName: ['', Validators.required],
      cliEntType: ['', Validators.required],
      cliEntDob: ['', Validators.required],
      cliEntAadhar: ['', [Validators.required, Validators.pattern(/^([0-9]){12}$/)]],
      cliEntUAadhar: ['', Validators.required],
      cliEntPan: ['', [Validators.required, Validators.pattern(/^([a-zA-Z]([a-zA-Z]([a-zA-Z]([a-zA-Z]([a-zA-Z]([0-9]([0-9]([0-9]([0-9]([a-zA-Z])?)?)?)?)?)?)?)?)?)?$/)]],
      cliEntCode: ['', Validators.required],
      cliEntConstituition: ['', Validators.required],
      cliEntLlc: ['', Validators.required],
      cliEntBusiness: ['', Validators.required],
      cliEntPwd: ['', Validators.required]
    });

    this.getOtpToMode = this._formBuilder.group({
      isEmail: 'false'
    });
    this.verifyOtpToMode = this._formBuilder.group({
      otpfield: ['', Validators.required]
    });

    // this.getMobSolo = this._formBuilder.group({
    //   cmobsolo: ''
    // });
    // if (this.getMobSolo) {
    //   this.getMobSolo = new FormGroup({
    //     cmobsolo: new FormControl(this.clientPhn)
    //   });
    // }
    // this.verfiyMobSolo = this._formBuilder.group({
    //   cmobotpsolo: ''
    // });
    // this.getMobSolo.controls['cmobsolo'].disable();

    // this.getEmailSolo = this._formBuilder.group({
    //   cemailsolo: ''
    // });
    // if (this.getEmailSolo) {
    //   this.getEmailSolo = new FormGroup({
    //     cemailsolo: new FormControl(this.clientEmail)
    //   });
    // }
    // this.verifyEmailSolo = this._formBuilder.group({
    //   cemailotpsolo: ''
    // });
    // this.getEmailSolo.controls['cemailsolo'].disable();

    this.clientEntity.get("cliEntDob").disable();
    this.clientEntity.get("cliEntAadhar").disable();
    this.clientEntity.get("cliEntUAadhar").disable();

    this.clientEntVerified = sessionStorage.getItem('clientVerified') ? JSON.parse(sessionStorage.getItem('clientVerified')) : false;


    if (this.clientEntityData) {
      this.clientEntity.get('cliEntName').setValue(this.clientEntityData.entityName);
      this.clientEntity.get('cliEntType').setValue(this.clientEntityData.entityType);
      this.clientEntity.get('cliEntDob').setValue(this.clientEntityData.dob);
      this.clientEntity.get('cliEntAadhar').setValue(this.clientEntityData.aadharNumber);
      this.clientEntity.get('cliEntUAadhar').setValue(this.clientEntityData.udyogAadhaar);
      this.clientEntity.get('cliEntPan').setValue(this.clientEntityData.pan);
      this.clientEntity.get('cliEntCode').setValue(this.clientEntityData.iecCode);
      this.clientEntity.get('cliEntConstituition').setValue(this.clientEntityData.constitution);
      this.clientEntity.get('cliEntLlc').setValue(this.clientEntityData.llP_CIN);
      this.clientEntity.get('cliEntBusiness').setValue(this.clientEntityData.natureofBusiness);
      this.clientEntity.get('cliEntPwd').setValue(this.clientEntityData.efilingPassword);

      this.clientEntity.get("cliEntName").disable();
      this.clientEntity.get("cliEntType").disable();

      if (this.clientEntityData.entityType === 'Individual') {
        this.individualField = true;
        this.clientEntity.get("cliEntDob").enable();
        this.clientEntity.get("cliEntAadhar").enable();
        this.clientEntity.get("cliEntUAadhar").enable();
      } else {
        this.individualField = false;
        this.clientEntity.get("cliEntDob").disable();
        this.clientEntity.get("cliEntAadhar").disable();
        this.clientEntity.get("cliEntUAadhar").disable();
      }
    }

  }


  toggleIndividualField() {
    if (this.clientEntity.value.cliEntType === 'Individual') {
      this.individualField = true;
      this.clientEntity.get("cliEntDob").enable();
      this.clientEntity.get("cliEntAadhar").enable();
      this.clientEntity.get("cliEntUAadhar").enable();
    } else {
      this.individualField = false;
      this.clientEntity.get("cliEntDob").disable();
      this.clientEntity.get("cliEntAadhar").disable();
      this.clientEntity.get("cliEntUAadhar").disable();
    }
  }


  submitCLientEntity() {
    this.cliEntyStatusFailure = false;
    if (this.clientEntity.invalid) {
      return;
    }
    this.showLoaderSubmit = true;
    let arg = this.clientEntity.getRawValue();

    console.log('sds', arg)

    if (!this.clientEntityData) {
      this.config.submitClientEntity(this.clientId, arg).subscribe(
        (response: any) => {
          if (response.success === true) {
            this.cliEntityStatus = true;
            this.showLoaderSubmit = false;
            setTimeout(() => {
              this.dialogRef.close()
            }, 2000);
          } else {
            this.showLoaderSubmit = false;
            this.RegOtpErrMsg = response.message;
            this.cliEntyStatusFailure = true;
          }

        },
        (error) => {
          this.showLoaderSubmit = false;
          this.RegOtpErrMsg = error.error.message;
          this.cliEntyStatusFailure = true;
        }
      )
    } else {
      this.config.modifyClientEntity(this.clientEntityData.id, this.clientId, arg).subscribe(
        (response: any) => {
          if (response.success === true) {
            this.cliEntityStatus = true;
            this.showLoaderSubmit = false;
            setTimeout(() => {
              this.dialogRef.close()
            }, 2000);
          } else {
            this.showLoaderSubmit = false;
            this.RegOtpErrMsg = response.message;
            this.cliEntyStatusFailure = true;
          }

        },
        (error) => {
          this.showLoaderSubmit = false;
          this.RegOtpErrMsg = error.error.message;
          this.cliEntyStatusFailure = true;
        }
      )
    }

  }

  
  getOtpToType() {
    this.showLoaderForMobSolo = true;
    console.log('this.clientId.id', this.clientId)
    const body = {
      "isToEditClient": true,
      "isEmail": JSON.parse(this.getOtpToMode.getRawValue().isEmail),
      "clientId": JSON.parse(this.clientId)
    }

    this.loginServ.getOTP(body).subscribe(
      (response: any) => {
        if (response.success === true) {
          this.showLoaderForMobSolo = false;
          this.otpReceived = true;
        } else {
          this.RegOtpErrOccured = true;
          this.RegOtpErrMsg = 'Please try again';
          this.showLoaderForMob = false;
        }

      },
      (error) => {
        this.RegOtpErrOccured = true;
        this.RegOtpErrMsg = 'Please try again';
        this.showLoaderForMob = false;
      }
    )
  }

  verifyOtpToType() {
    // stop here if form is invalid
    this.RegOtpErrOccured = false;
    if (this.verifyOtpToMode.invalid) {
      return;
    }
    this.showLoaderForMob = true;

    const body = {
      "isToEditClient": true,
      "isEmail": JSON.parse(this.getOtpToMode.getRawValue().isEmail),
      "otp": this.verifyOtpToMode.value.otpfield,
      "clientId": JSON.parse(this.clientId)
    }

    this.config.getOtpVerify(body).subscribe(
      (response: any) => {
        if (response.body.success === true) {
          this.isMobVerified = true;
          this.RegOtpErrOccured = false;
          this.showLoaderForMob = false;
          this.otpVerified = true;
          sessionStorage.setItem('clientVerified', 'true');
          setTimeout(() => {
            this.clientEntVerified = true;
          }, 2000);
        } else {
          this.RegOtpErrOccured = true;
          this.RegOtpErrMsg = 'Please enter correct OTP and try again';
          this.showLoaderForMob = false;
        }

      },
      (error) => {
        this.RegOtpErrOccured = true;
        this.RegOtpErrMsg = 'Please enter correct OTP and try again';
        this.showLoaderForMob = false;
      }
    )
  }

  // getClientMobileOtp() {
  //   this.showLoaderForMobSolo = true;

  //   const body = {
  //     "isToEditClient": true,
  //     "phone": this.getMobSolo.getRawValue().cmobsolo
  //   }

  //   this.loginServ.getOTP(body).subscribe(
  //     (response: any) => {
  //       if (response.success === true) {
  //         this.showLoaderForMobSolo = false;
  //         this.otpReceived = true;
  //       } else {
  //         this.RegOtpErrOccured = true;
  //         this.RegOtpErrMsg = 'Please try again';
  //         this.showLoaderForMob = false;
  //       }

  //     },
  //     (error) => {
  //       this.RegOtpErrOccured = true;
  //       this.RegOtpErrMsg = 'Please try again';
  //       this.showLoaderForMob = false;
  //     }
  //   )
  // }

  // verifyClientMobileSolo() {
  //   // stop here if form is invalid
  //   if (this.verfiyMobSolo.invalid) {
  //     return;
  //   }
  //   this.showLoaderForMob = true;

  //   const body = {
  //     "isToEditClient": true,
  //     "isEmail": false,
  //     "phone": this.clientPhn,
  //     "otp": this.verfiyMobSolo.value.cmobotpsolo
  //   }

  //   this.config.getOtpVerify(body).subscribe(
  //     (response: any) => {
  //       if (response.body.success === true) {
  //         this.isMobVerified = true;
  //         this.RegOtpErrOccured = false;
  //         this.showLoaderForMob = false;
  //         this.otpVerified = true;
  //         sessionStorage.setItem('clientVerified', 'true');
  //         setTimeout(() => {
  //           this.clientEntVerified = true;
  //         }, 2000);
  //       } else {
  //         this.RegOtpErrOccured = true;
  //         this.RegOtpErrMsg = 'Please enter correct OTP and try again';
  //         this.showLoaderForMob = false;
  //       }

  //     },
  //     (error) => {
  //       this.RegOtpErrOccured = true;
  //       this.RegOtpErrMsg = 'Please enter correct OTP and try again';
  //       this.showLoaderForMob = false;
  //     }
  //   )
  // }


  // getClientEmailOtp() {
  //   this.showLoaderForMobSolo = true;

  //   const body = {
  //     "isToEditClient": true,
  //     "isEmail": true,
  //     "email": this.getEmailSolo.getRawValue().cemailsolo
  //   }

  //   this.loginServ.getOTP(body).subscribe(
  //     (response: any) => {
  //       if (response.success === true) {
  //         this.showLoaderForMobSolo = false;
  //         this.otpReceived = true;
  //       } else {
  //         this.RegOtpErrOccured = true;
  //         this.RegOtpErrMsg = 'Please try again';
  //         this.showLoaderForMob = false;
  //       }

  //     },
  //     (error) => {
  //       this.RegOtpErrOccured = true;
  //       this.RegOtpErrMsg = 'Please try again';
  //       this.showLoaderForMob = false;
  //     }
  //   )
  // }

  // verifyClientEmailSolo() {
  //   // stop here if form is invalid
  //   if (this.verifyEmailSolo.invalid) {
  //     return;
  //   }
  //   this.showLoaderForMob = true;

  //   const body = {
  //     "isToEditClient": true,
  //     "isEmail": true,
  //     "email": this.clientEmail,
  //     "otp": this.verifyEmailSolo.value.cemailotpsolo
  //   }

  //   this.config.getOtpVerify(body).subscribe(
  //     (response: any) => {
  //       if (response.body.success === true) {
  //         this.isMobVerified = true;
  //         this.RegOtpErrOccured = false;
  //         this.showLoaderForMob = false;
  //         this.otpVerified = true;
  //         sessionStorage.setItem('clientVerified', 'true');
  //         setTimeout(() => {
  //           this.clientEntVerified = true;
  //         }, 2000);
  //       } else {
  //         this.RegOtpErrOccured = true;
  //         this.RegOtpErrMsg = 'Please enter correct OTP and try again';
  //         this.showLoaderForMob = false;
  //       }

  //     },
  //     (error) => {
  //       this.RegOtpErrOccured = true;
  //       this.RegOtpErrMsg = 'Please enter correct OTP and try again';
  //       this.showLoaderForMob = false;
  //     }
  //   )
  // }

}

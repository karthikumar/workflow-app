import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntityBranchDialogComponent } from './entity-branch-dialog.component';

describe('EntityBranchDialogComponent', () => {
  let component: EntityBranchDialogComponent;
  let fixture: ComponentFixture<EntityBranchDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntityBranchDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntityBranchDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

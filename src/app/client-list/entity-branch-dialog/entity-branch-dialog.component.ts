import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ConfigService } from 'src/app/config.service';
import { LoginService } from 'src/app/login/services/login.service';
import { MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-entity-branch-dialog',
  templateUrl: './entity-branch-dialog.component.html',
  styleUrls: ['./entity-branch-dialog.component.scss']
})
export class EntityBranchDialogComponent implements OnInit {



  public clientId;
  public entityId;
  public clientPhn;
  public clientEmail;
  public entityBranchData;
  cliEntityStatus;
  // public getMobSolo;
  // public getEmailSolo;
  // public verfiyMobSolo;
  // public verifyEmailSolo;

  fieldValErr;
  RegOtpErrMsg;
  showLoaderSubmit: boolean = false;
  cliEntyStatusFailure: boolean = false;
  individualField: boolean = false;
  clientEntVerified: boolean;
  otpReceived: boolean = false;
  showLoaderForMobSolo: boolean = false;
  isMobVerified: boolean = false;
  showLoaderForMob: boolean = false;
  RegOtpErrOccured: boolean = false;
  otpVerified: boolean = false;
  public verifyOtpToMode;
  public getOtpToMode;

  entityBranch = new FormGroup({
    entBranchName: new FormControl(''),
    entTanNum: new FormControl(''),
    entTanTracesId: new FormControl(''),
    entTdsUserId: new FormControl(''),
    entTanEfillingId: new FormControl(''),
    entTdsUserIdTax: new FormControl(''),
    entTanEfillingPwd: new FormControl(''),
    entPtrcNum: new FormControl(''),
    entPtrcId: new FormControl(''),
    entGstNum: new FormControl(''),
    branchGstLoginId: new FormControl(''),
    branchGstPwd: new FormControl(''),
    branchEwayBillId: new FormControl(''),
    branchGstEwayBillPwd: new FormControl(''),
    branchGstEmail: new FormControl(''),
    branchGstMob: new FormControl(''),
    branchAddrOne: new FormControl(''),
    branchAddrTwo: new FormControl(''),
    branchLocality: new FormControl(''),
    branchCity: new FormControl(''),
    branchState: new FormControl(''),
    branchCountry: new FormControl(''),
    branchZip: new FormControl('')
  });

  constructor(private _formBuilder: FormBuilder,
    private config: ConfigService,
    private loginServ: LoginService,
    private dialogRef: MatDialogRef<EntityBranchDialogComponent>) {
  }

  ngOnInit(): void {
    this.entityBranch = this._formBuilder.group({
      entBranchName: ['', Validators.required],
      entTanNum: ['', [Validators.required, Validators.pattern(/^[0-9]+$/)]],
      entTanTracesId: ['', Validators.required],
      entTdsUserId: ['', Validators.required],
      entTanEfillingId: ['', Validators.required],
      entTdsUserIdTax: ['', Validators.required],
      entTanEfillingPwd: ['', Validators.required],
      entPtrcNum: ['', [Validators.required, Validators.pattern(/^[0-9]+$/)]],
      entPtrcId: ['', Validators.required],
      entGstNum: ['', [Validators.required, Validators.pattern(/^[0-9]+$/)]],
      branchGstLoginId: ['', Validators.required],
      branchGstPwd: ['', Validators.required],
      branchEwayBillId: ['', Validators.required],
      branchGstEwayBillPwd: ['', Validators.required],
      branchGstEmail: ['', [Validators.required, Validators.email]],
      branchGstMob: ['', [Validators.required, Validators.pattern(/^[6-9]\d{9}$/)]],
      branchAddrOne: ['', Validators.required],
      branchAddrTwo: ['', Validators.required],
      branchLocality: ['', Validators.required],
      branchCity: ['', Validators.required],
      branchState: ['', Validators.required],
      branchCountry: ['', Validators.required],
      branchZip: ['', Validators.required]
    });
    
    this.getOtpToMode = this._formBuilder.group({
      isEmail: 'false'
    });
    this.verifyOtpToMode = this._formBuilder.group({
      otpfield: ['', Validators.required]
    });



    // this.getMobSolo = this._formBuilder.group({
    //   cmobsolo: ''
    // });
    // if (this.getMobSolo) {
    //   this.getMobSolo = new FormGroup({
    //     cmobsolo: new FormControl(this.clientPhn)
    //   });
    // }
    // this.verfiyMobSolo = this._formBuilder.group({
    //   cmobotpsolo: ''
    // });
    // this.getMobSolo.controls['cmobsolo'].disable();

    // this.getEmailSolo = this._formBuilder.group({
    //   cemailsolo: ''
    // });
    // if (this.getEmailSolo) {
    //   this.getEmailSolo = new FormGroup({
    //     cemailsolo: new FormControl(this.clientEmail)
    //   });
    // }
    // this.verifyEmailSolo = this._formBuilder.group({
    //   cemailotpsolo: ''
    // });
    // this.getEmailSolo.controls['cemailsolo'].disable();


    this.clientEntVerified = sessionStorage.getItem('clientVerified') ? JSON.parse(sessionStorage.getItem('clientVerified')) : false;


    if (this.entityBranchData) {
      this.entityBranch.get('entBranchName').setValue(this.entityBranchData.branchName);
      this.entityBranch.get('entTanNum').setValue(this.entityBranchData.tanNumber);
      this.entityBranch.get('entTanTracesId').setValue(this.entityBranchData.entTanTracesId);
      this.entityBranch.get('entTdsUserId').setValue(this.entityBranchData.tdsUserIdTrace);
      this.entityBranch.get('entTanEfillingId').setValue(this.entityBranchData.tanEfilingId);
      this.entityBranch.get('entTdsUserIdTax').setValue(this.entityBranchData.tdsUserIdTax);
      this.entityBranch.get('entTanEfillingPwd').setValue(this.entityBranchData.tanEfilingPassword);
      this.entityBranch.get('entPtrcNum').setValue(this.entityBranchData.ptrcNumber);
      this.entityBranch.get('entPtrcId').setValue(this.entityBranchData.ptrcId);
      this.entityBranch.get('entGstNum').setValue(this.entityBranchData.gstNumber);
      this.entityBranch.get('branchGstLoginId').setValue(this.entityBranchData.gstLoginId);
      this.entityBranch.get('branchGstPwd').setValue(this.entityBranchData.gstPassword);
      this.entityBranch.get('branchEwayBillId').setValue(this.entityBranchData.gstEwayBillId);
      this.entityBranch.get('branchGstEwayBillPwd').setValue(this.entityBranchData.gstEwayBillPassword);
      this.entityBranch.get('branchGstEmail').setValue(this.entityBranchData.gstEmail);
      this.entityBranch.get('branchGstMob').setValue(this.entityBranchData.branchGstMob);
      this.entityBranch.get('branchAddrOne').setValue(this.entityBranchData.address1);
      this.entityBranch.get('branchAddrTwo').setValue(this.entityBranchData.address2);
      this.entityBranch.get('branchLocality').setValue(this.entityBranchData.locality);
      this.entityBranch.get('branchCity').setValue(this.entityBranchData.city);
      this.entityBranch.get('branchState').setValue(this.entityBranchData.state);
      this.entityBranch.get('branchCountry').setValue(this.entityBranchData.country);
      this.entityBranch.get('branchZip').setValue(this.entityBranchData.zip);
      this.entityBranch.get("entBranchName").disable();

    }

  }

  submitEntityBranch() {
    let arg = this.entityBranch.getRawValue();
    this.cliEntyStatusFailure = false;
    if (this.entityBranch.invalid) {
      return;
    }
    this.showLoaderSubmit = true;

    if (!this.entityBranchData) {
      this.config.submitEntityBranch(this.entityId, arg).subscribe(
        (response: any) => {
          if (response.success === true) {
            this.cliEntityStatus = true;
            this.showLoaderSubmit = false;
            setTimeout(() => {
              this.dialogRef.close()
            }, 2000);
          } else {
            this.showLoaderSubmit = false;
            this.RegOtpErrMsg = response.message;
            this.cliEntyStatusFailure = true;
          }

        },
        (error) => {
          this.showLoaderSubmit = false;
          this.RegOtpErrMsg = error.error.message;
          this.cliEntyStatusFailure = true;
        }
      )
    } else {
      this.config.modifyEntityBranch(this.entityBranchData.id, this.entityBranchData.entityId, arg).subscribe(
        (response: any) => {
          if (response.success === true) {
            this.cliEntityStatus = true;
            this.showLoaderSubmit = false;
            setTimeout(() => {
              this.dialogRef.close()
            }, 2000);
          } else {
            this.showLoaderSubmit = false;
            this.RegOtpErrMsg = response.message;
            this.cliEntyStatusFailure = true;
          }

        },
        (error) => {
          this.showLoaderSubmit = false;
          this.RegOtpErrMsg = error.error.message;
          this.cliEntyStatusFailure = true;
        }
      )
    }

  }

  
  getOtpToType() {
    this.showLoaderForMobSolo = true;
    console.log('this.clientId.id', this.clientId)
    const body = {
      "isToEditClient": true,
      "isEmail": JSON.parse(this.getOtpToMode.getRawValue().isEmail),
      "clientId": JSON.parse(this.clientId)
    }

    this.loginServ.getOTP(body).subscribe(
      (response: any) => {
        if (response.success === true) {
          this.showLoaderForMobSolo = false;
          this.otpReceived = true;
        } else {
          this.RegOtpErrOccured = true;
          this.RegOtpErrMsg = 'Please try again';
          this.showLoaderForMob = false;
        }

      },
      (error) => {
        this.RegOtpErrOccured = true;
        this.RegOtpErrMsg = 'Please try again';
        this.showLoaderForMob = false;
      }
    )
  }

  verifyOtpToType() {
    // stop here if form is invalid
    this.RegOtpErrOccured = false;
    if (this.verifyOtpToMode.invalid) {
      return;
    }
    this.showLoaderForMob = true;

    const body = {
      "isToEditClient": true,
      "isEmail": JSON.parse(this.getOtpToMode.getRawValue().isEmail),
      "otp": this.verifyOtpToMode.value.otpfield,
      "clientId": JSON.parse(this.clientId)
    }

    this.config.getOtpVerify(body).subscribe(
      (response: any) => {
        if (response.body.success === true) {
          this.isMobVerified = true;
          this.RegOtpErrOccured = false;
          this.showLoaderForMob = false;
          this.otpVerified = true;
          sessionStorage.setItem('clientVerified', 'true');
          setTimeout(() => {
            this.clientEntVerified = true;
          }, 2000);
        } else {
          this.RegOtpErrOccured = true;
          this.RegOtpErrMsg = 'Please enter correct OTP and try again';
          this.showLoaderForMob = false;
        }

      },
      (error) => {
        this.RegOtpErrOccured = true;
        this.RegOtpErrMsg = 'Please enter correct OTP and try again';
        this.showLoaderForMob = false;
      }
    )
  }


  // getClientMobileOtp() {
  //   this.showLoaderForMobSolo = true;

  //   const body = {
  //     "isToEditClient": true,
  //     "phone": this.getMobSolo.getRawValue().cmobsolo
  //   }

  //   this.loginServ.getOTP(body).subscribe(
  //     (response: any) => {
  //       if (response.success === true) {
  //         this.showLoaderForMobSolo = false;
  //         this.otpReceived = true;
  //       } else {
  //         this.RegOtpErrOccured = true;
  //         this.RegOtpErrMsg = 'Please try again';
  //         this.showLoaderForMob = false;
  //       }

  //     },
  //     (error) => {
  //       this.RegOtpErrOccured = true;
  //       this.RegOtpErrMsg = 'Please try again';
  //       this.showLoaderForMob = false;
  //     }
  //   )
  // }

  // verifyClientMobileSolo() {
  //   // stop here if form is invalid
  //   if (this.verfiyMobSolo.invalid) {
  //     return;
  //   }
  //   this.showLoaderForMob = true;

  //   const body = {
  //     "isToEditClient": true,
  //     "isEmail": false,
  //     "phone": this.clientPhn,
  //     "otp": this.verfiyMobSolo.value.cmobotpsolo
  //   }

  //   this.config.getOtpVerify(body).subscribe(
  //     (response: any) => {
  //       if (response.body.success === true) {
  //         this.isMobVerified = true;
  //         this.RegOtpErrOccured = false;
  //         this.showLoaderForMob = false;
  //         this.otpVerified = true;
  //         sessionStorage.setItem('clientVerified', 'true');
  //         setTimeout(() => {
  //           this.clientEntVerified = true;
  //         }, 2000);
  //       } else {
  //         this.RegOtpErrOccured = true;
  //         this.RegOtpErrMsg = 'Please enter correct OTP and try again';
  //         this.showLoaderForMob = false;
  //       }

  //     },
  //     (error) => {
  //       this.RegOtpErrOccured = true;
  //       this.RegOtpErrMsg = 'Please enter correct OTP and try again';
  //       this.showLoaderForMob = false;
  //     }
  //   )
  // }


  // getClientEmailOtp() {
  //   this.showLoaderForMobSolo = true;

  //   const body = {
  //     "isToEditClient": true,
  //     "isEmail": true,
  //     "email": this.getEmailSolo.getRawValue().cemailsolo
  //   }

  //   this.loginServ.getOTP(body).subscribe(
  //     (response: any) => {
  //       if (response.success === true) {
  //         this.showLoaderForMobSolo = false;
  //         this.otpReceived = true;
  //       } else {
  //         this.RegOtpErrOccured = true;
  //         this.RegOtpErrMsg = 'Please try again';
  //         this.showLoaderForMob = false;
  //       }

  //     },
  //     (error) => {
  //       this.RegOtpErrOccured = true;
  //       this.RegOtpErrMsg = 'Please try again';
  //       this.showLoaderForMob = false;
  //     }
  //   )
  // }

  // verifyClientEmailSolo() {
  //   // stop here if form is invalid
  //   if (this.verifyEmailSolo.invalid) {
  //     return;
  //   }
  //   this.showLoaderForMob = true;

  //   const body = {
  //     "isToEditClient": true,
  //     "isEmail": true,
  //     "email": this.clientEmail,
  //     "otp": this.verifyEmailSolo.value.cemailotpsolo
  //   }

  //   this.config.getOtpVerify(body).subscribe(
  //     (response: any) => {
  //       if (response.body.success === true) {
  //         this.isMobVerified = true;
  //         this.RegOtpErrOccured = false;
  //         this.showLoaderForMob = false;
  //         this.otpVerified = true;
  //         sessionStorage.setItem('clientVerified', 'true');
  //         setTimeout(() => {
  //           this.clientEntVerified = true;
  //         }, 2000);
  //       } else {
  //         this.RegOtpErrOccured = true;
  //         this.RegOtpErrMsg = 'Please enter correct OTP and try again';
  //         this.showLoaderForMob = false;
  //       }

  //     },
  //     (error) => {
  //       this.RegOtpErrOccured = true;
  //       this.RegOtpErrMsg = 'Please enter correct OTP and try again';
  //       this.showLoaderForMob = false;
  //     }
  //   )
  // }

}

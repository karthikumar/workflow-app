import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntityBranchComponent } from './entity-branch.component';

describe('EntityBranchComponent', () => {
  let component: EntityBranchComponent;
  let fixture: ComponentFixture<EntityBranchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntityBranchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntityBranchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

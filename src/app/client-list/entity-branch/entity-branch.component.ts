import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ConfigService } from 'src/app/config.service';
import { MatTableDataSource } from '@angular/material/table';
import { EntityBranchDialogComponent } from '../entity-branch-dialog/entity-branch-dialog.component';

@Component({
  selector: 'app-entity-branch',
  templateUrl: './entity-branch.component.html',
  styleUrls: ['./entity-branch.component.scss']
})
export class EntityBranchComponent implements OnInit {


  displayedColumns: string[] =
    [
      'branchName',
      'tanNumber',
      'ptrcNumber',
      'gstNumber',
      'gstEmail',
      'gstMobile',
      'action'
    ];
  public dataSource;
  public entityId;
  public entityName;
  public clientId;
  public clientPhn;
  public clientEmail;
  isError: boolean = false;
  isLoadingResults: boolean = true;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private route: ActivatedRoute,
    public dialog: MatDialog,
    private config: ConfigService,
    public router: Router) {
    this.clientId = this.route.snapshot.paramMap.get('cid');
    this.entityId = this.route.snapshot.paramMap.get('EntId');
    console.log(this.clientId, this.entityId)
  }

  ngOnInit(): void {
    this.clientId = this.route.snapshot.paramMap.get('cid');
    this.entityId = this.route.snapshot.paramMap.get('EntId');
    this.getEntityBranchList();
    this.getClient();
  }

  goToBranch(id) {
    this.router.navigateByUrl('/entitybank/' + this.clientId + '/' + id);
  }

  openDialog(entityBranchData) {
    const dialogRef = this.dialog.open(EntityBranchDialogComponent, {
      width: '60%'
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getEntityBranchList();
    });
    dialogRef.componentInstance.entityId = this.entityId;
    dialogRef.componentInstance.clientId = this.clientId;
    dialogRef.componentInstance.clientPhn = this.clientPhn;
    dialogRef.componentInstance.clientEmail = this.clientEmail;
    dialogRef.componentInstance.entityBranchData = entityBranchData;

  }

  getClient() {
    this.config.getClient(this.clientId).subscribe(
      (response: any) => {
        if (response.success === true && response.data) {
          this.clientPhn = response.data.phone;
          this.clientEmail = response.data.email;
        } else {
          // this.isError = true;
        }

      },
      (error) => {
        // this.RegErrOccured = true;
        // this.RegErrMsg = error.error.message;
        // this.otpRequested = false;
        // this.showLoader = false;
      }
    )
  }

  deleteEnt(name, id) {
    if (confirm("Are you sure to delete '"+name+"' branch? ")) {
      this.isLoadingResults = true;  
      this.config.deleteEnt(id).subscribe(
        (response: any) => {
          if (response.success === true) {
            this.isLoadingResults = false;
            this.getEntityBranchList();
          } else {
            // this.isError = true;
          }

        },
        (error) => {
          // this.RegErrOccured = true;
          // this.RegErrMsg = error.error.message;
          // this.otpRequested = false;
          // this.showLoader = false;
        }
      )
    }
  }


  getEntityBranchList() {
    this.isLoadingResults = true;
    this.config.getEntitiesBranchList(this.entityId).subscribe(
      (response: any) => {
        if (response.success === true && response.data.length > 0) {
          this.entityName = response.data.filter(x => x && x.entityName)[0].entityName;
          this.isLoadingResults = false;
          this.isError = false;
          this.dataSource = new MatTableDataSource(response.data);
        } else {
          this.isError = true;
        }

      },
      (error) => {
        // this.RegErrOccured = true;
        // this.RegErrMsg = error.error.message;
        // this.otpRequested = false;
        // this.showLoader = false;
      }
    )
  }

  modifyEntityBranch(
    id,
    entityId,
    entityName,
    branchName,
    tanNumber,
    tanTracesId,
    tdsUserIdTrace,
    tanEfilingId,
    tdsUserIdTax,
    tanEfilingPassword,
    ptrcNumber,
    ptrcId,
    gstNumber,
    gstLoginId,
    gstPassword,
    gstEwayBillId,
    gstEwayBillPassword,
    gstEmail,
    gstMobile,
    address1,
    address2,
    locality,
    city,
    state,
    country,
    zip) {
    let entityBranchData = {
      "id": parseInt(id),
      "entityId": parseInt(entityId),
      "entityName": entityName,
      "branchName": branchName,
      "tanNumber": tanNumber,
      "tanTracesId": tanTracesId,
      "tdsUserIdTrace": tdsUserIdTrace,
      "tanEfilingId": tanEfilingId,
      "tdsUserIdTax": tdsUserIdTax,
      "tanEfilingPassword": tanEfilingPassword,
      "ptrcNumber": ptrcNumber,
      "ptrcId": ptrcId,
      "gstNumber": gstNumber,
      "gstLoginId": gstLoginId,
      "gstPassword": gstPassword,
      "gstEwayBillId": gstEwayBillId,
      "gstEwayBillPassword": gstEwayBillPassword,
      "gstEmail": gstEmail,
      "gstMobile": gstMobile,
      "address1": address1,
      "address2": address2,
      "locality": locality,
      "city": city,
      "state": state,
      "country": country,
      "zip": zip
    }
    this.openDialog(entityBranchData);
  }
}

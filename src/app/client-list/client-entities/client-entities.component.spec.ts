import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientEntitiesComponent } from './client-entities.component';

describe('ClientEntitiesComponent', () => {
  let component: ClientEntitiesComponent;
  let fixture: ComponentFixture<ClientEntitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientEntitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientEntitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

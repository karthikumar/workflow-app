import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { ClientEntitiesDialogComponent } from '../client-entities-dialog/client-entities-dialog.component';
import { ConfigService } from 'src/app/config.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-client-entities',
  templateUrl: './client-entities.component.html',
  styleUrls: ['./client-entities.component.scss']
})
export class ClientEntitiesComponent implements OnInit {
  displayedColumns: string[] =
    [
      'entityName',
      'constitution',
      'entityType',
      'pan',
      'iecCode',
      'llP_CIN',
      'action'
    ];
  public dataSource;
  public clientId;
  public clientName;
  public clientPhn;
  public clientEmail;
  isError: boolean = false;
  isLoadingResults: boolean = true;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private route: ActivatedRoute,
    public dialog: MatDialog,
    private config: ConfigService,
    public router: Router) { }

  ngOnInit(): void {
    this.clientId = this.route.snapshot.paramMap.get('cid');
    this.getClientEntityList();
    this.getClient();
  }

  goToBranch(id) {
    this.router.navigateByUrl('/entitybranch/' + this.clientId + '/' + id);
  }

  openDialog(clientEntityData) {
    const dialogRef = this.dialog.open(ClientEntitiesDialogComponent, {
      width: '60%'
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getClientEntityList();
    });
    dialogRef.componentInstance.clientId = this.clientId;
    dialogRef.componentInstance.clientPhn = this.clientPhn;
    dialogRef.componentInstance.clientEmail = this.clientEmail;
    dialogRef.componentInstance.clientEntityData = clientEntityData;

  }

  getClient() {
    this.config.getClient(this.clientId).subscribe(
      (response: any) => {
        if (response.success === true && response.data) {
          this.clientName = response.data.title + ' ' + response.data.firstName + ' ' + response.data.middleName + ' ' + response.data.lastName;
          this.clientPhn = response.data.phone;
          this.clientEmail = response.data.email;
        } else {
          // this.isError = true;
        }

      },
      (error) => {
        // this.RegErrOccured = true;
        // this.RegErrMsg = error.error.message;
        // this.otpRequested = false;
        // this.showLoader = false;
      }
    )
  }

  getClientEntityList() {
    this.isLoadingResults = true;
    this.config.getClientEntitiesList(this.clientId).subscribe(
      (response: any) => {
        if (response.success === true && response.data.length > 0) {
          this.isLoadingResults = false;
          this.isError = false;
          this.dataSource = new MatTableDataSource(response.data);
        } else {
          this.isError = true;
        }

      },
      (error) => {
        // this.RegErrOccured = true;
        // this.RegErrMsg = error.error.message;
        // this.otpRequested = false;
        // this.showLoader = false;
      }
    )
  }

  deactivate(name, id) {
    if (confirm("Are you sure to deactivate '" + name + "'? ")) {
      this.isLoadingResults = true;
      this.config.deactivateEntity(id).subscribe(
        (response: any) => {
          if (response.success === true) {
            this.isLoadingResults = false;
            this.isError = false;
            this.getClientEntityList();
          } else {
            this.isError = true;
          }

        },
        (error) => {
          // this.RegErrOccured = true;
          // this.RegErrMsg = error.error.message;
          // this.otpRequested = false;
          // this.showLoader = false;
        }
      )
    }
  }

  activateEnt(name, id) {
    if (confirm("Are you sure to activate '" + name + "'? ")) {
      this.isLoadingResults = true;
      this.config.activateEntity(id).subscribe(
        (response: any) => {
          if (response.success === true) {
            this.isLoadingResults = false;
            this.isError = false;
            this.getClientEntityList();
          } else {
            this.isError = true;
          }

        },
        (error) => {
          // this.RegErrOccured = true;
          // this.RegErrMsg = error.error.message;
          // this.otpRequested = false;
          // this.showLoader = false;
        }
      )
    }
  }
  modifyClientEntity(id, clientId, entityName, entityType, pan, iecCode, constitution, llP_CIN, dob, aadharNumber, udyogAadhaar, natureofBusiness, efilingPassword) {
    console.log(id, clientId, entityName, entityType, pan, iecCode, constitution, llP_CIN, dob, aadharNumber, udyogAadhaar, natureofBusiness, efilingPassword)
    let clientEntityData = {
      "id": id,
      "clientId": clientId,
      "entityName": entityName,
      "entityType": entityType,
      "pan": pan,
      "iecCode": iecCode,
      "constitution": constitution,
      "llP_CIN": llP_CIN,
      "dob": dob,
      "aadharNumber": aadharNumber,
      "udyogAadhaar": udyogAadhaar,
      "natureofBusiness": natureofBusiness,
      "efilingPassword": efilingPassword
    }
    this.openDialog(clientEntityData);
  }

}


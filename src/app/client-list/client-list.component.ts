import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { ClientListDialogComponent } from './client-list-dialog/client-list-dialog.component';
import { MatSort } from '@angular/material/sort';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { merge, Observable, of as observableOf } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss']
})
export class ClientListComponent implements OnInit {
  displayedColumns: string[] = ['name', 'phone', 'email', 'action'];

  clientListDb: ObtainClientList | null;
  data: clientList[] = [];
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  constructor(private _httpClient: HttpClient, public dialog: MatDialog,
    private config: ConfigService, private router: Router) { }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.getClientList();
  }

  getClientList() {
    this.clientListDb = new ObtainClientList(this._httpClient);
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.clientListDb!.getclientList(
            this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          if (data && data.data.length > 0) {
            this.isLoadingResults = false;
            this.isRateLimitReached = false;
            this.resultsLength = data.rowCount;
            return data.data;

          } else {
            this.isLoadingResults = false;
            this.isRateLimitReached = true;
          }
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the GitHub API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      ).subscribe((data: any) => this.data = data);
  }

  openDialog(type, val) {
    const dialogRef = this.dialog.open(ClientListDialogComponent, {
      width: '60%'
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getClientList();
    });
    dialogRef.componentInstance.modelType = type;
    dialogRef.componentInstance.modelVal = val;
  }
  goToEntity(id) {
    this.router.navigateByUrl('/cliententity/' + id);
  }
  modifyClient(id) {
    let clientDetails = this.data.filter(x => x.id === id)[0];
    this.openDialog('modify', clientDetails);
  }

}


export interface clientListApi {
  data: clientList[];
  rowCount: number;
}

export interface clientList {
  id: number;
  serviceId: number;
  serviceName: string
  financialYearId: 2
  financialYearName: string
  baseFee: string;
  increaseBasis: string;
  baseThreshold: string;
  step: number;
  increasePerStep: number;
}

/** An example database that the data source uses to retrieve data for the table. */
export class ObtainClientList {
  constructor(private _httpClient: HttpClient) { }

  env: any = environment;

  getclientList(sort: string, order: string, page: number, size: number): Observable<clientListApi> {
    const href = this.env.getClientListUrl;
    const requestUrl = `${href}?currentPage=${page + 1}&pageSize=${size}`;
    return this._httpClient.get<clientListApi>(requestUrl);
  }
}
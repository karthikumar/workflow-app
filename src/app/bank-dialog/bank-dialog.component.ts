import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ConfigService } from '../config.service';
import { LoginService } from '../login/services/login.service';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-bank-dialog',
  templateUrl: './bank-dialog.component.html',
  styleUrls: ['./bank-dialog.component.scss']
})
export class BankDialogComponent implements OnInit {



  public clientId;
  public branchId;
  public clientPhn;
  public clientEmail;
  public bankData;
  entityBankStatus;
  // public getMobSolo;
  // public getEmailSolo;
  // public verfiyMobSolo;
  // public verifyEmailSolo;
  public verifyOtpToMode;
  public getOtpToMode;

  fieldValErr;
  RegOtpErrMsg;
  showLoaderSubmit: boolean = false;
  bankStatusFailure: boolean = false;
  individualField: boolean = false;
  clientEntVerified: boolean;
  otpReceived: boolean = false;
  showLoaderForMobSolo: boolean = false;
  isMobVerified: boolean = false;
  showLoaderForMob: boolean = false;
  RegOtpErrOccured: boolean = false;
  otpVerified: boolean = false;

  entityBank = new FormGroup({
    bankBranchName: new FormControl(''),
    bankName: new FormControl(''),
    accountName: new FormControl(''),
    accountNumber: new FormControl(''),
    customerId: new FormControl(''),
    iBankPassword: new FormControl(''),
    corporateId: new FormControl(''),
    corporateUserId: new FormControl(''),
    closureDate: new FormControl('')
  });

  constructor(private _formBuilder: FormBuilder,
    private config: ConfigService,
    private loginServ: LoginService,
    private dialogRef: MatDialogRef<BankDialogComponent>) {
  }

  ngOnInit(): void {
    this.entityBank = this._formBuilder.group({
      bankBranchName: ['', Validators.required],
      bankName: ['', Validators.required],
      accountName: ['', Validators.required],
      accountNumber: ['', [Validators.required, Validators.pattern(/^[0-9]+$/)]],
      customerId: ['', Validators.required],
      iBankPassword: ['', Validators.required],
      corporateId: ['', Validators.required],
      corporateUserId: ['', Validators.required],
      closureDate: '',
    });


    this.getOtpToMode = this._formBuilder.group({
      isEmail: 'false'
    });
    this.verifyOtpToMode = this._formBuilder.group({
      otpfield: ['', Validators.required]
    });


    // this.getMobSolo = this._formBuilder.group({
    //   cmobsolo: ''
    // });
    // if (this.getMobSolo) {
    //   this.getMobSolo = new FormGroup({
    //     cmobsolo: new FormControl(this.clientPhn)
    //   });
    // }
    // this.verfiyMobSolo = this._formBuilder.group({
    //   cmobotpsolo: ''
    // });
    // this.getMobSolo.controls['cmobsolo'].disable();

    // this.getEmailSolo = this._formBuilder.group({
    //   cemailsolo: ''
    // });
    // if (this.getEmailSolo) {
    //   this.getEmailSolo = new FormGroup({
    //     cemailsolo: new FormControl(this.clientEmail)
    //   });
    // }
    // this.verifyEmailSolo = this._formBuilder.group({
    //   cemailotpsolo: ''
    // });
    // this.getEmailSolo.controls['cemailsolo'].disable();


    this.clientEntVerified = sessionStorage.getItem('clientVerified') ? JSON.parse(sessionStorage.getItem('clientVerified')) : false;


    if (this.bankData) {
      this.entityBank = new FormGroup({
        bankBranchName: new FormControl(this.bankData.bankBranchName),
        bankName: new FormControl(this.bankData.bankName),
        accountName: new FormControl(this.bankData.accountName),
        accountNumber: new FormControl(this.bankData.accountNumber),
        customerId: new FormControl(this.bankData.customerId),
        iBankPassword: new FormControl(this.bankData.iBankPassword),
        corporateId: new FormControl(this.bankData.corporateId),
        corporateUserId: new FormControl(this.bankData.corporateUserId),
        closureDate: new FormControl(this.bankData.closureDate)
      });

    }

  }

  submitBankData() {
    let arg = this.entityBank.getRawValue();
    this.bankStatusFailure = false;
    if (this.entityBank.invalid) {
      return;
    }
    this.showLoaderSubmit = true;

    if (!this.bankData) {
      this.config.submitBankData(this.branchId, arg).subscribe(
        (response: any) => {
          if (response.success === true) {
            this.entityBankStatus = true;
            this.showLoaderSubmit = false;
            setTimeout(() => {
              this.dialogRef.close()
            }, 2000);
          } else {
            this.showLoaderSubmit = false;
            this.RegOtpErrMsg = response.message;
            this.bankStatusFailure = true;
          }

        },
        (error) => {
          this.showLoaderSubmit = false;
          this.RegOtpErrMsg = error.error.message;
          this.bankStatusFailure = true;
        }
      )
    } else {
      this.config.modifyBankData(this.bankData.id, this.bankData.branchId, arg).subscribe(
        (response: any) => {
          if (response.success === true) {
            this.entityBankStatus = true;
            this.showLoaderSubmit = false;
            setTimeout(() => {
              this.dialogRef.close()
            }, 2000);
          } else {
            this.showLoaderSubmit = false;
            this.RegOtpErrMsg = response.message;
            this.bankStatusFailure = true;
          }

        },
        (error) => {
          this.showLoaderSubmit = false;
          this.RegOtpErrMsg = error.error.message;
          this.bankStatusFailure = true;
        }
      )
    }

  }

  getOtpToType() {
    this.showLoaderForMobSolo = true;
    console.log('this.clientId.id', this.clientId)
    const body = {
      "isToEditClient": true,
      "isEmail": JSON.parse(this.getOtpToMode.getRawValue().isEmail),
      "clientId": JSON.parse(this.clientId)
    }

    this.loginServ.getOTP(body).subscribe(
      (response: any) => {
        if (response.success === true) {
          this.showLoaderForMobSolo = false;
          this.otpReceived = true;
        } else {
          this.RegOtpErrOccured = true;
          this.RegOtpErrMsg = 'Please try again';
          this.showLoaderForMob = false;
        }

      },
      (error) => {
        this.RegOtpErrOccured = true;
        this.RegOtpErrMsg = 'Please try again';
        this.showLoaderForMob = false;
      }
    )
  }

  verifyOtpToType() {
    // stop here if form is invalid
    this.RegOtpErrOccured = false;
    if (this.verifyOtpToMode.invalid) {
      return;
    }
    this.showLoaderForMob = true;

    const body = {
      "isToEditClient": true,
      "isEmail": JSON.parse(this.getOtpToMode.getRawValue().isEmail),
      "otp": this.verifyOtpToMode.value.otpfield,
      "clientId": JSON.parse(this.clientId)
    }

    this.config.getOtpVerify(body).subscribe(
      (response: any) => {
        if (response.body.success === true) {
          this.isMobVerified = true;
          this.RegOtpErrOccured = false;
          this.showLoaderForMob = false;
          this.otpVerified = true;
          sessionStorage.setItem('clientVerified', 'true');
          setTimeout(() => {
            this.clientEntVerified = true;
          }, 2000);
        } else {
          this.RegOtpErrOccured = true;
          this.RegOtpErrMsg = 'Please enter correct OTP and try again';
          this.showLoaderForMob = false;
        }

      },
      (error) => {
        this.RegOtpErrOccured = true;
        this.RegOtpErrMsg = 'Please enter correct OTP and try again';
        this.showLoaderForMob = false;
      }
    )
  }

  // getClientMobileOtp() {
  //   this.showLoaderForMobSolo = true;

  //   const body = {
  //     "isToEditClient": true,
  //     "phone": this.getMobSolo.getRawValue().cmobsolo
  //   }

  //   this.loginServ.getOTP(body).subscribe(
  //     (response: any) => {
  //       if (response.success === true) {
  //         this.showLoaderForMobSolo = false;
  //         this.otpReceived = true;
  //       } else {
  //         this.RegOtpErrOccured = true;
  //         this.RegOtpErrMsg = 'Please try again';
  //         this.showLoaderForMob = false;
  //       }

  //     },
  //     (error) => {
  //       this.RegOtpErrOccured = true;
  //       this.RegOtpErrMsg = 'Please try again';
  //       this.showLoaderForMob = false;
  //     }
  //   )
  // }

  // verifyClientMobileSolo() {
  //   // stop here if form is invalid
  //   if (this.verfiyMobSolo.invalid) {
  //     return;
  //   }
  //   this.showLoaderForMob = true;

  //   const body = {
  //     "isToEditClient": true,
  //     "isEmail": false,
  //     "phone": this.clientPhn,
  //     "otp": this.verfiyMobSolo.value.cmobotpsolo
  //   }

  //   this.config.getOtpVerify(body).subscribe(
  //     (response: any) => {
  //       if (response.body.success === true) {
  //         this.isMobVerified = true;
  //         this.RegOtpErrOccured = false;
  //         this.showLoaderForMob = false;
  //         this.otpVerified = true;
  //         sessionStorage.setItem('clientVerified', 'true');
  //         setTimeout(() => {
  //           this.clientEntVerified = true;
  //         }, 2000);
  //       } else {
  //         this.RegOtpErrOccured = true;
  //         this.RegOtpErrMsg = 'Please enter correct OTP and try again';
  //         this.showLoaderForMob = false;
  //       }

  //     },
  //     (error) => {
  //       this.RegOtpErrOccured = true;
  //       this.RegOtpErrMsg = 'Please enter correct OTP and try again';
  //       this.showLoaderForMob = false;
  //     }
  //   )
  // }


  // getClientEmailOtp() {
  //   this.showLoaderForMobSolo = true;

  //   const body = {
  //     "isToEditClient": true,
  //     "isEmail": true,
  //     "email": this.getEmailSolo.getRawValue().cemailsolo
  //   }

  //   this.loginServ.getOTP(body).subscribe(
  //     (response: any) => {
  //       if (response.success === true) {
  //         this.showLoaderForMobSolo = false;
  //         this.otpReceived = true;
  //       } else {
  //         this.RegOtpErrOccured = true;
  //         this.RegOtpErrMsg = 'Please try again';
  //         this.showLoaderForMob = false;
  //       }

  //     },
  //     (error) => {
  //       this.RegOtpErrOccured = true;
  //       this.RegOtpErrMsg = 'Please try again';
  //       this.showLoaderForMob = false;
  //     }
  //   )
  // }

  // verifyClientEmailSolo() {
  //   // stop here if form is invalid
  //   if (this.verifyEmailSolo.invalid) {
  //     return;
  //   }
  //   this.showLoaderForMob = true;

  //   const body = {
  //     "isToEditClient": true,
  //     "isEmail": true,
  //     "email": this.clientEmail,
  //     "otp": this.verifyEmailSolo.value.cemailotpsolo
  //   }

  //   this.config.getOtpVerify(body).subscribe(
  //     (response: any) => {
  //       if (response.body.success === true) {
  //         this.isMobVerified = true;
  //         this.RegOtpErrOccured = false;
  //         this.showLoaderForMob = false;
  //         this.otpVerified = true;
  //         sessionStorage.setItem('clientVerified', 'true');
  //         setTimeout(() => {
  //           this.clientEntVerified = true;
  //         }, 2000);
  //       } else {
  //         this.RegOtpErrOccured = true;
  //         this.RegOtpErrMsg = 'Please enter correct OTP and try again';
  //         this.showLoaderForMob = false;
  //       }

  //     },
  //     (error) => {
  //       this.RegOtpErrOccured = true;
  //       this.RegOtpErrMsg = 'Please enter correct OTP and try again';
  //       this.showLoaderForMob = false;
  //     }
  //   )
  // }

}

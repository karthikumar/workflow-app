import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { LoginService } from './services/login.service'
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})

export class LoginComponent implements OnInit {

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  public otplogin = true;
  public logintitle = "MEMBER lOGIN"
  public loginErrOccured: boolean = false;
  public loginErrMsg: any;
  public loginOTPErrOccured: boolean = false;
  public showLoader: boolean = false;
  public loginOTPSuccess: boolean = false;
  public loginOTPSubmitAreaVisible: boolean = false;
  public loginOTPMsg: any;
  loading = false;
  submitted = false;
  loginData: {};
  public loginOTPData: {};
  constructor(
    private _formBuilder: FormBuilder,
    private loginService: LoginService,
    private router: Router,) { }

  goForward(stepper: MatStepper, openOtp) {
    this.otplogin = openOtp;
    if (openOtp == false) {
      this.logintitle = "RESET PASSWORD";
    } else {
      this.logintitle = "MEMEBER lOGIN";
    }
    stepper.next();
  }

  ngOnInit(): void {
    this.firstFormGroup = this._formBuilder.group({
      userid: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      phone: ['', Validators.required],
      otp: ['']
    });

    this.thirdFormGroup = this._formBuilder.group({
      resetemail: ['', Validators.required]
    });
    localStorage.clear();
  }
  // convenience getter for easy access to form fields
  get f() { return this.firstFormGroup.controls; }

  // Function for clicking Login
  onLoginSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.firstFormGroup.invalid) {
      return;
    }

    this.loading = true;
    this.showLoader = true;
    this.loginData = {
      username: this.f.userid.value,
      password: this.f.password.value
    }
    this.loginService.login(this.loginData).subscribe(
      (response: any) => {
        this.loading = false;

        if (response.success == false) {
          this.loginErrOccured = true;
          this.loginErrMsg = response.message;
        }
        else {
          this.loginService.setUser(response.body.data.username);
          localStorage.setItem('token', response.headers.get('token'));
          this.router.navigate(['/dashboard']);
        }
      },
      (error) => {
        //this.alertService.error(error);
        this.loginErrOccured = true;
        this.loginErrMsg = 'Something went wrong';
        this.loading = false;
        this.showLoader = false;
      });
  }

  // convenience getter for easy access to form fields
  get g() { return this.secondFormGroup.controls; }

  // Function for clicking Login with OTP
  onLoginGetOTPSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.secondFormGroup.invalid) {
      return;
    }

    this.loading = true;

    if (this.g.phone.value.indexOf('@') > -1) {
      this.loginOTPData = {
        email: this.g.phone.value,
        isEmail: true,
        isTologin: true
      }
    }
    else {
      this.loginOTPData = {
        phone: this.g.phone.value,
        isTologin: true
      }
    }
    this.loginService.getOTP(this.loginOTPData).subscribe(
      (response: any) => {
        this.loading = false;
        if (response.success == false) {
          this.loginOTPSubmitAreaVisible = false;
          this.loginOTPErrOccured = true;
          this.loginOTPSuccess = false;
          this.loginOTPMsg = response.message;
        } else {
          this.loginOTPSubmitAreaVisible = true;
          this.loginOTPSuccess = true;
          this.loginOTPErrOccured = false;
          this.loginOTPMsg = response.message;
        }
      },
      (error) => {
        this.loginOTPSubmitAreaVisible = false;
        this.loginOTPErrOccured = true;
        this.loginOTPSuccess = false;
        this.loginOTPMsg = error.error.message;
        this.loading = false;
        this.showLoader = false;
      });
  }


  onLoginOTPVerify() {
    this.loading = true;
    // stop here if form is invalid
    if (!this.secondFormGroup.value.otp) {
      alert('');
      return;
    }
    if (this.g.phone.value.indexOf('@') > -1) {
      this.loginOTPData = {
        email: this.g.phone.value,
        isEmail: true,
        isTologin: true,
        otp: this.secondFormGroup.value.otp
      }
    }
    else {
      this.loginOTPData = {
        phone: this.g.phone.value,
        isTologin: true,
        isEmail: false,
        otp: this.secondFormGroup.value.otp
      }
    }

    this.loginService.loginVerifyOTP(this.loginOTPData).subscribe(
      (response: any) => {
        this.loading = false;
        if (response.success == false) {
          this.loginOTPErrOccured = true;
          this.loginOTPSuccess = false;
          this.loginOTPMsg = response.message;
        } else {
          this.loginOTPSuccess = true;
          this.loginOTPErrOccured = false;
          this.loginOTPMsg = response.message;

          if (response.data.isEmailVerified && response.data.isPhoneVerified) {
            this.router.navigate(['/setPassword']);

          }
          else {
            this.router.navigate(['/verifyUser']);
          }

        }
      },
      (error) => {
        this.loginOTPErrOccured = true;
        this.loginOTPSuccess = false;
        this.loginOTPMsg = error.error.message;
        this.loading = false;
        this.showLoader = false;
      });
  }
  // convenience getter for easy access to form fields
  get h() { return this.thirdFormGroup.controls; }

  resetPassword() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.thirdFormGroup.invalid) {
      return;
    }
  }
}
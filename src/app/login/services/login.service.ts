import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private http: HttpClient
  ) { }

  login(data) {
    return this.http.post<object>(`http://localhost:44339/api/authentication/authenticate`, data, { observe: 'response' });    
  }

  getOTP(data) {
    return this.http.post<any>(`http://localhost:44339/api/authentication/getotp`, data);
  }

  loginVerifyOTP(data) {
    var result = this.http.post<object>(`http://localhost:44339/api/authentication/verifyotp`, data);
    return result;
  }
  public getUser(): string {
    return localStorage.getItem('wfmuser');
  }

  public setUser(arg) {
    localStorage.setItem('wfmuser', arg);
  }

  public getToken(): string {
    return localStorage.getItem('token');
  }

  public setToken(response) {
    var token = response.headers.get('token');
    if (token == null) {
      return;
    }

    if (localStorage.getItem('token') != null) {
      localStorage.removeItem('token');
    }

    localStorage.setItem('token', response.headers.get('token'));
  }

}
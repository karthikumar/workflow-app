import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfigService } from '../config.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-verify-user',
  templateUrl: './verify-user.component.html',
  styleUrls: ['./verify-user.component.scss']
})
export class VerifyUserComponent implements OnInit {
  verifyUserFormGroup: FormGroup;
  public enableSetPwd: boolean = false;
  public isEmailVerified: boolean = false;
  public isMobVerified: boolean = false;
  public otpRequested: boolean = false;
  public emailOtpRequested: boolean = false;
  public mobOtpRequested: boolean = false;
  public loginErrOccured: boolean = false;
  public loginErrMsg: any;
  public loginOTPErrOccured: boolean = false;
  public loginOTPSuccess: boolean = false;
  public loginOTPMsg: any;
  public showLoader: boolean = false;
  public RegErrOccured: boolean = false;
  public RegOtpErrOccured: boolean = false;
  public RegPwdErrOccured: boolean = false;
  public showLoaderForEmail: boolean = false;
  public showLoaderForMob: boolean = false;
  public userRegistered: boolean = false;
  public userPwdSet: boolean = false;
  public RegErrMsg: any;
  public RegOtpErrMsg: any;
  public RegPwdErrMsg: any;
  constructor(
    private _formBuilder: FormBuilder,
    private httpClient: HttpClient,
    private config: ConfigService
  ) { }

  ngOnInit(): void {
    this.verifyUserFormGroup = this._formBuilder.group({
      emailotp: ['', Validators.required],
      mobotp: ['', Validators.required]
    });
  }

  verifyEmail() {
    this.emailOtpRequested = true;
    this.showLoaderForEmail = true;
    const body = {
      "IsToVerify": true,
      "IsEmail": true,
      "Email": sessionStorage.getItem('regEmail'),
      "Otp": this.verifyUserFormGroup.value.emailotp
    }

    this.config.getOtpVerify(body).subscribe(
      (response: any) => {
        if (response.body.success === true) {
          this.isEmailVerified = true;
          this.showLoaderForEmail = false;
          if (this.isEmailVerified && this.isMobVerified) {
            this.enableSetPwd = true;
            let headerToken = response.headers.get('token');
            sessionStorage.setItem('regToken', headerToken);
          }
        } else {
          this.RegOtpErrOccured = true;
          this.RegOtpErrMsg = response.body.message ? response.body.message : 'Please try again';
          this.emailOtpRequested = false;
          this.showLoaderForEmail = false;
        }

      },
      (error) => {
        this.RegOtpErrOccured = true;
        this.RegOtpErrMsg = error.error.message ? error.error.message : 'Please try again';
        this.emailOtpRequested = false;
        this.showLoaderForEmail = false;
      }
    )
  }

  verifyMobile() {

    this.mobOtpRequested = true;
    this.showLoaderForMob = true;

    const body = {
      "IsToVerify": true,
      "IsEmail": false,
      "Phone": sessionStorage.getItem('regPhone'),
      "Otp": this.verifyUserFormGroup.value.mobotp
    }

    this.config.getOtpVerify(body).subscribe(
      (response : any) => {
        if (response.body.success === true) {
          this.isMobVerified = true;
          this.showLoaderForMob = false;
          if (this.isEmailVerified && this.isMobVerified) {
            this.enableSetPwd = true;
            let headerToken = response.headers.get('token');
            sessionStorage.setItem('regToken', headerToken);
          }
        } else {
          this.RegOtpErrOccured = true;
          this.RegOtpErrMsg = response.body.message ? response.body.message : 'Please try again';
          this.mobOtpRequested = false;
          this.showLoaderForMob = false;
        }

      },
      (error) => {
        this.RegOtpErrOccured = true;
        this.RegOtpErrMsg = error.error.message ? error.error.message : 'Please try again';
        this.mobOtpRequested = false;
        this.showLoaderForMob = false;
      }
    )
  }

}

import { Component, OnInit } from '@angular/core';
import {MatDialogModule} from '@angular/material/dialog';

@Component({
  selector: 'app-task-data-dialog',
  templateUrl: './task-data-dialog.component.html',
  styleUrls: ['./task-data-dialog.component.scss']
})
export class TaskDataDialogComponent implements OnInit {

  validationValueField: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }
  showValField() {
    this.validationValueField = true;
  }
}

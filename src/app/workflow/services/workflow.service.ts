import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WorkflowService {

  env:any = environment;

  constructor(
    private httpClient: HttpClient
  ) { }

  getMasterData() {
    return this.httpClient.get(this.env.masterdataUrl);
  }
  getPeriodData(id: number) {
    return this.httpClient.get(this.env.periodUrl + '/' + id);
  }
  getWorkflowData(id: number) {
    return this.httpClient.get(this.env.workflowByIdUrl+ '/' + id);
  }
  createWorkflow(data) {
      return this.httpClient.post(this.env.workflowCreateUrl, data);
  }
  modifyWorkflow(data) {
    return this.httpClient.post(this.env.workflowModifyUrl, data);
  }
}

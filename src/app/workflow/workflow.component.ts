import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TaskDataDialogComponent } from './task-data-dialog/task-data-dialog.component';
import { MatStepper } from '@angular/material/stepper';
import { ActivatedRoute, Router } from '@angular/router';
import { WorkflowService } from './services/workflow.service';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-workflow',
  templateUrl: './workflow.component.html',
  styleUrls: ['./workflow.component.scss']
})
export class WorkflowComponent implements OnInit {
  workflowid: number;
  periodid: number;
  serviceid: number;
  masterData: any;
  periodsData: any;
  displayType: any;
  ownerRole: any;
  actionType: any;
  actionEntityList: any;
  taskDataType: any;
  taskDataSelections: any;
  taskValidationTypes: any;
  workflowData: any;
  pageLoaded: boolean = false;
  public workflowForm: any;


  @ViewChild('stepperHor') stepperHor: MatStepper;
  constructor(
    private router: Router,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private workflowService: WorkflowService,
    private formBuilder: FormBuilder) {
    this.periodid = +this.route.snapshot.paramMap.get('periodid');
    this.serviceid = +this.route.snapshot.paramMap.get('serviceid');
    this.workflowid = +this.route.snapshot.paramMap.get('workflowid');
  }

  openDialog() {
    const dialogRef = this.dialog.open(TaskDataDialogComponent, {
      width: '60%',
      height: "90%"
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  ngOnInit(): void {

    const workflowDatarequest = this.workflowService.getWorkflowData(this.workflowid);
    const masterDatarequest = this.workflowService.getMasterData();
    const periodsDatarequest = this.workflowService.getPeriodData(this.periodid);

    forkJoin([masterDatarequest, periodsDatarequest, workflowDatarequest]).subscribe((response) => {
      this.masterData = response[0];
      this.displayType = this.masterData.data.find(x => x.listName == 'TaskDisplayTypes').listValues;
      this.ownerRole = this.masterData.data.find(x => x.listName == 'UserRoles').listValues;
      this.actionType = this.masterData.data.find(x => x.listName == 'TaskActionTypes')?.listValues;
      this.actionEntityList = this.masterData.data.find(x => x.listName == 'TaskActionEntities').listValues;
      this.taskDataSelections = this.masterData.data.find(x => x.listName == 'TaskDataSelections').listValues;
      this.taskDataType = this.masterData.data.find(x => x.listName == 'TaskDataTypes').listValues;
      this.taskValidationTypes = this.masterData.data.find(x => x.listName == 'TaskValidationTypes').listValues;
      this.periodsData = response[1];
      this.pageLoaded = true;
      this.workflowData = response[2]
      if (this.workflowData.success) {
        this.workflowForm = this.buildForm(this.workflowData.data);
      }
      else {
        this.workflowForm = this.buildForm(null);
      }
    });
  }

  buildForm(data: any): FormGroup {
    return data ? this.formBuilder.group({
      id: [data.id ? data.id : ''],
      periodId: [this.periodid],
      name: [data.name ? data.name : '', Validators.required],
      isRetrospective: [data.isRetrospective, Validators.required],
      isSequential: [data.isSequential, Validators.required],
      tasks: this.formBuilder.array(
        this.getTaskForm(data.tasks ? data.tasks : null)
      ),
    })
      :
      this.formBuilder.group({
        id: [0],
        periodId: [this.periodid],
        name: ['', Validators.required],
        isRetrospective: [false, Validators.required],
        isSequential: [false, Validators.required],
        tasks: this.formBuilder.array(
          this.getTaskForm(null)
        ),
      });

  }


  get tasks() { return this.workflowForm.get('tasks'); }

  taskDatas(taskid, taskDataId): FormArray {
    return this.tasks.controls[taskid].get('taskDatas').controls[taskDataId].get('selectionData');
  }

  addTask() {
    if (this.workflowForm.get('tasks').valid) {
      this.tasks.push(this.getTaskForm(null)[0]);
    }
    else {
      this.workflowForm.markAllAsTouched();
      alert('Please enter valid tasks');
    }
  }

  availableSelections(taskid, taskDataId, event) {
    event.value.forEach(function (value) {
      this.tasks.controls[taskid].get('taskDatas').controls[taskDataId].get('selectionData').push(this.availableSelectionsFormGroup(value))
    });
  }

  private createSelectionDataForm(data): FormGroup[] {
    return data ?
      data.map(x => {
        this.formBuilder.group({
          Name: [x.Name ? x.Name : '', Validators.required],
          Value: [x.Value ? x.Value : '', Validators.required],
        })
      }) : [
        this.formBuilder.group({
          Name: ['', Validators.required],
          Value: ['', Validators.required],
        })];
  }


  private fillSelectionDataForm(data): FormGroup[] {
    return data ?
      data.map(x => {
        this.formBuilder.group({
          Name: [x.Name ? x.Name : '', Validators.required],
          Value: [x.Value ? x.Value : '', Validators.required],
        })
      }) : [];
  }

  private createValidationForm(data): FormGroup[] {
    return data ?
      data.map(x => {
        this.formBuilder.group({
          Name: [x.Name ? x.Name : '', Validators.required],
          Value: [x.Value ? x.Value : '', Validators.required],
        })
      }) :
      [this.formBuilder.group({
        validationTypeId: [null, Validators.required],
        validationValue: [''],
      })];
  }
  private fillValidationForm(data): FormGroup[] {
    return data ?
      data.map(x => {
        this.formBuilder.group({
          Name: [x.Name ? x.Name : '', Validators.required],
          Value: [x.Value ? x.Value : '', Validators.required],
        })
      }) :
      [];
  }
  removetaskData(taskid, taskDataId) {
    this.tasks.controls[taskid].get('taskDatas').removeAt(taskDataId);
  }
  groupType(taskid, taskDataId, value) {
    if (value) {
      this.tasks.controls[taskid].get('members').controls = [];
    } else {
      this.tasks.controls[taskid].get('members').controls = [];
    }
  }
  removeValidation(taskid, taskDataId, validationId) {
    this.tasks.controls[taskid].get('taskDatas').controls[taskDataId].get('validationData').removeAt(validationId);
  }
  addValidation(taskid, taskDataId) {
    if (this.tasks.controls[taskid].get('taskDatas').controls[taskDataId].get('validationData').valid) {
      this.tasks.controls[taskid].get('taskDatas').controls[taskDataId].get('validationData').push(this.createValidationForm(null)[0])
    } else {
      alert('Please enter valid validation');
    }
  }
  removeSelectionDataForm(taskid, taskDataId, selectionDataId) {
    this.tasks.controls[taskid].get('taskDatas').controls[taskDataId].get('selectionData').removeAt(selectionDataId);
  }
  addSelectionDataForm(taskid, taskDataId) {
    if (this.tasks.controls[taskid].get('taskDatas').controls[taskDataId].get('selectionData').valid) {
      this.tasks.controls[taskid].get('taskDatas').controls[taskDataId].get('selectionData').push(this.createSelectionDataForm(null)[0])
    } else {
      alert('Please enter valid Selection Data');
    }
  }
  addTaskData(id: number) {
    if (this.tasks.controls[id].get('taskDatas').valid) {
      this.tasks.controls[id].get('taskDatas').push(this.getTaskDatas(null)[0]);
    } else {
      this.workflowForm.markAllAsTouched();
      alert('Please enter valid task Datas');
    }
  }

  removeMembersTaskData(taskid, taskDataId, membersTaskDataId) {
    this.tasks.controls[taskid].get('taskDatas').controls[taskDataId].get('members').removeAt(membersTaskDataId);
  }
  removeMembersSelectionDataForm(taskid, taskDataId, memberstaskDataId, selectionDataId) {
    this.tasks.controls[taskid].get('taskDatas').controls[taskDataId].get('members').controls[memberstaskDataId].get('selectionData').removeAt(selectionDataId);
  }
  addMembersSelectionDataForm(taskid, taskDataId, memberstaskDataId) {
    if (this.tasks.controls[taskid].get('taskDatas').controls[taskDataId].get('members').controls[memberstaskDataId].get('selectionData').valid) {
      this.tasks.controls[taskid].get('taskDatas').controls[taskDataId].get('members').controls[memberstaskDataId].get('selectionData').push(this.createSelectionDataForm(null)[0])
    } else {
      alert('Please enter valid Selection Data');
    }
  }
  removeMembersValidation(taskid, taskDataId, memberstaskDataId, validationId) {

    this.tasks.controls[taskid].get('taskDatas').controls[taskDataId].get('members').controls[memberstaskDataId].get('validationData').removeAt(validationId);
  }
  addMembersValidation(taskid, taskDataId, memberstaskDataId) {
    if (this.tasks.controls[taskid].get('taskDatas').controls[taskDataId].get('members').controls[memberstaskDataId].get('validationData').valid) {
      this.tasks.controls[taskid].get('taskDatas').controls[taskDataId].get('members').controls[memberstaskDataId].get('validationData').push(this.createValidationForm(null)[0])
    } else {
      alert('Please enter valid validation');
    }
  }
  addMemberTaskData(taskid, taskDataId) {
    if (this.tasks.controls[taskid].get('taskDatas').controls[taskDataId].get('members').valid) {
      this.tasks.controls[taskid].get('taskDatas').controls[taskDataId].get('members').push(this.getTaskDatas(null)[0]);
    }
    else {
      this.workflowForm.markAllAsTouched();
      alert('Please enter valid Member data');
    }
  }

  removeTask(id: number) {
    const index = this.tasks.indexOf(id);
    if (index > -1) {
      this.tasks.splice(index, 1);
    }
  }

  private getTaskForm(data: any[] | null): FormGroup[] {
    return data ?
      data.map(x => {
        return this.formBuilder.group({
          id: [x.id ? x.id : 0],
          name: [x.name ? x.name : '', Validators.required],
          description: [x.description ? x.description : '', Validators.required],
          displayTypeId: [x.displayTypeId ? x?.displayTypeId : 0, Validators.required],
          ownerRoles: [x.ownerRoles ? x.ownerRoles : 0, Validators.required],
          // sequence: [x.sequence ? x.sequence : 0, Validators.required],
          taskDatas: this.formBuilder.array(
            this.getTaskDatas(x.taskDatas ? x.taskDatas : null)
          )
        })
      })
      :
      [this.formBuilder.group({
        id: [0],
        name: ['', Validators.required],
        description: ['', Validators.required],
        displayTypeId: ['', Validators.required],
        ownerRoles: ['', Validators.required],
        //sequence: [0, Validators.required],
        taskDatas: this.formBuilder.array(
          this.getTaskDatas(null)
        )
      })];
  }

  private getTaskDatas(data: any[] | null): FormGroup[] {
    return data ?
      data.map(x => {
        return this.formBuilder.group({
          id: [x.id],
          name: [x.name ? x.name : '', Validators.required],
          description: [x.description ? x.description : '', Validators.required],
          isGroupType: [x.members ? (x.members.length > 0 ? true : false) : false],
          isRepeatable: [x.isRepeatable ? x.isRepeatable : false],
          // sequence: [x.sequence ? x.sequence : 0, Validators.required],
          isActionType: [x.isActionType ? x.isActionType : false],
          actionTypeId: [x.actionTypeId ? x.actionTypeId : 0],
          actionEntity: [x.actionEntity ? x.actionEntity : ''],
          taskDataTypeId: [x.taskDataTypeId ? x.taskDataTypeId : 0],
          selectionData: this.formBuilder.array(this.fillSelectionDataForm(x.selectionData ? x.selectionData : null)),
          validationData: this.formBuilder.array(this.fillValidationForm(x.validationData ? x.validationData : null)),
          members: this.formBuilder.array(x.members && x.members.length > 0  ?this.fillTaskDatas(x.members ? x.members : null):[]),
        })
      })
      : [this.formBuilder.group({
        id: [0],
        name: ['', Validators.required],
        description: ['', Validators.required],
        isGroupType: [false],
        isRepeatable: [false],
        // sequence: [0, Validators.required],
        isActionType: [false],
        actionTypeId: [0],
        actionEntity: [''],
        taskDataTypeId: [null],
        selectionData: this.formBuilder.array([
        ]),
        validationData: this.formBuilder.array([
        ]),
        members: this.formBuilder.array([
        ]),
      })];
  }

  private fillTaskDatas(data: any[] | null): FormGroup[] {
    return data ?
      data.map(x => {
        return this.formBuilder.group({
          id: [x.id],
          name: [x.name ? x.name : '', Validators.required],
          description: [x.description ? x.description : '', Validators.required],
          isGroupType: [x.members ? (x.members.length > 0 ? true : false) : false],
          isRepeatable: [x.isRepeatable ? x.isRepeatable : false],
          // sequence: [x.sequence ? x.sequence : 0, Validators.required],
          isActionType: [x.isActionType ? x.isActionType : false],
          actionTypeId: [x.actionTypeId ? x.actionTypeId : 0],
          actionEntity: [x.actionEntity ? x.actionEntity : ''],
          taskDataTypeId: [x.taskDataTypeId ? x.taskDataTypeId : 0],
          selectionData: this.formBuilder.array(this.fillSelectionDataForm(x.selectionData ? x.selectionData : null)),
          validationData: this.formBuilder.array(this.fillValidationForm(x.validationData ? x.validationData : null)),
          members: this.formBuilder.array(x.members && x.members.length > 0  ?this.fillTaskDatas(x.members ? x.members : null):[]),
        })
      })
      : [];
  }

  getTaskName(value) {
    if (value == '') {
      return 'Task Data'
    }
    else {
      return value;
    }
  }
  getTaskDataType(value) {
    if (!value) {
      return value
    }
    else {
      return this.taskDataType.find(x => x.id === value).value
    }
  }


  ngAfterViewInit() {

  }
  validateNext(stepper: MatStepper) {
    if (this.workflowForm.controls.name.status == "INVALID") {
      this.workflowForm.controls['name'].markAsTouched()
      return;
    }
    stepper.next();
  }


  submitWorkflow() {
    if (this.workflowForm.invalid) {
      this.workflowForm.markAllAsTouched();
      return;
    }

    var formData = this.workflowForm.value;
    formData.serviceId = this.serviceid;
    if (formData.id > 0) {
      formData.modifyTasks = formData.tasks?.filter(x => x.id > 0);
      formData.addTasks = formData.tasks?.filter(x => x.id <= 0);
      formData.tasks = null;
      this.workflowService.modifyWorkflow(formData).subscribe((x: any) => {
        if (x.success) {
          this.router.navigateByUrl('/service/' + this.serviceid);
        }
        else {
          alert(x.message);
        }
      });
    }
    else {
      this.workflowService.createWorkflow(formData).subscribe((x: any) => {
        if (x.success) {
          this.router.navigateByUrl('/service/' + this.serviceid);
        }
        else {
          alert(x.message);
        }
      });
    }
  }
}

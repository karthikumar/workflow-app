import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { MatStepperModule } from '@angular/material/stepper';
import { MatRadioModule } from '@angular/material/radio';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { HeaderComponent } from './header/header.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CreateprofComponent } from './createprof/createprof.component';
import { HttpInterceptorService } from './services/http-interceptor.service';
import { VerifyUserComponent } from './verify-user/verify-user.component';
import { SetPasswordComponent } from './set-password/set-password.component';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyModule } from '@ngx-formly/core';
import { TaskComponent } from './task/task.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { UielementsComponent } from './uielements/uielements.component';
import { SampleformComponent } from './sampleform/sampleform.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { PageloaderComponent } from './pageloader/pageloader.component';
import { UidisabledelementComponent } from './uidisabledelement/uidisabledelement.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatMenuModule } from '@angular/material/menu';
import { AdminserviceComponent } from './adminservice/adminservice.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { WorkflowComponent } from './workflow/workflow.component';
import { TextFieldModule } from '@angular/cdk/text-field';
import { MatExpansionModule } from '@angular/material/expansion';
import { TaskDataDialogComponent } from './workflow/task-data-dialog/task-data-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTreeModule } from '@angular/material/tree';
import { AngularTreeGridModule } from 'angular-tree-grid';
import { MatTabsModule } from '@angular/material/tabs';
import { PeriodListComponent } from './period-list/period-list.component';
import { AdminServiceEditComponent } from './admin-service-edit/admin-service-edit.component';
import {MatButtonModule} from '@angular/material/button';
import { FeesChartComponent } from './fees-chart/fees-chart.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { FeesChartDialogComponent } from './fees-chart/fees-chart-dialog/fees-chart-dialog.component';
import { ClientListDialogComponent } from './client-list/client-list-dialog/client-list-dialog.component';
import { ClientListComponent } from './client-list/client-list.component';
import { MatSortModule } from '@angular/material/sort';
import { ClientEntitiesComponent } from './client-list/client-entities/client-entities.component';
import { ClientEntitiesDialogComponent } from './client-list/client-entities-dialog/client-entities-dialog.component';
import { EntityBranchComponent } from './client-list/entity-branch/entity-branch.component';
import { EntityBranchDialogComponent } from './client-list/entity-branch-dialog/entity-branch-dialog.component';
import { BankComponent } from './bank/bank.component';
import { BankDialogComponent } from './bank-dialog/bank-dialog.component';
import { QuotationComponent } from './quotation/quotation.component';
import { QuotationDialogComponent } from './quotation/quotation-dialog/quotation-dialog.component';
import { SleekWorkflowComponent } from './sleek-workflow/sleek-workflow.component';
import { SleekWorkflowDialogComponent } from './sleek-workflow/sleek-workflow-dialog/sleek-workflow-dialog.component';
import { ServicesLandingComponent } from './services-landing/services-landing.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    DashboardComponent,
    CreateprofComponent,
    VerifyUserComponent,
    SetPasswordComponent,
    TaskComponent,
    UielementsComponent,
    SampleformComponent,
    PageloaderComponent,
    UidisabledelementComponent,
    AdminserviceComponent,
    WorkflowComponent,
    TaskDataDialogComponent,
    PeriodListComponent,
    AdminServiceEditComponent,
    FeesChartComponent,
    FeesChartDialogComponent,
    ClientListComponent,
    ClientListDialogComponent,
    ClientEntitiesComponent,
    ClientEntitiesDialogComponent,
    EntityBranchComponent,
    EntityBranchDialogComponent,
    BankComponent,
    BankDialogComponent,
    QuotationComponent,
    QuotationDialogComponent,
    SleekWorkflowComponent,
    SleekWorkflowDialogComponent,
    ServicesLandingComponent 
  ],
  imports: [
    AngularTreeGridModule,
    MatExpansionModule,
    MatTabsModule,
    BrowserModule,
    MatSelectModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MatTableModule,
    MatTreeModule,
    ReactiveFormsModule,
    TextFieldModule,
    MatStepperModule,
    HttpClientModule,
    MatSortModule,
    MatRadioModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    FormlyBootstrapModule,
    MatInputModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMenuModule,
    MatCheckboxModule,
    MatDialogModule,
    MatButtonModule,
    MatPaginatorModule,
    FormlyModule.forRoot({
      validationMessages: [
        { name: 'required', message: 'This field is required' },
      ],
    }),
  ],
  exports: [MatFormFieldModule, MatInputModule],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: true }
    },
    { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    WorkflowComponent
  ]
})
export class AppModule { }

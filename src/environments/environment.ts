// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  registerUrl: "http://localhost:44339/api/users/register",
  otpVerifyUrl: "http://localhost:44339/api/Authentication/verifyOTP",
  pwdSetUrl: "http://localhost:44339/api/users/setuserpassword",
  tasksUrl: "http://localhost:44339/api/tasks/getallocatedtasks",
  taskElementsUrl: "http://localhost:44339/api/tasks/allocation",
  completedTasksUrl: "http://localhost:44339/api/tasks/getcompletedtasks",
  submitTaskDataUrl: "http://localhost:44339/api/tasks/submittaskdata",
  complatedTaskDataUrl: "http://localhost:44339/api/tasks/allocation",
  fileUploadUrl: "http://localhost:44339/api/tasks/{id}/upload",
  getAllServices: "http://localhost:44339/api/service/getservices",
  workflowUrl: "http://localhost:44339/api/workflow/getworkflows",
  workflowCreateUrl: "http://localhost:44339/api/workflow/create",
  workflowModifyUrl: "http://localhost:44339/api/workflow/modify",
  workflowByIdUrl: "http://localhost:44339/api/workflow",
  masterdataUrl: "http://localhost:44339/api/admin/masterdata",
  servicesUrl: "http://localhost:44339/api/service/getservices",
  periodUrl: "http://localhost:44339/api/service/period",
  feesChartUrl: "http://localhost:44339/api/feeschart/create",
  modifyFeesChartUrl: "http://localhost:44339/api/feeschart/modify",
  getFeesChartUrl: "http://localhost:44339/api/feeschart/getfeescharts",
  getClientListUrl: "http://localhost:44339/api/clients/getclients",
  getClientEntitiesListUrl: "http://localhost:44339/api/entities/getcliententities",
  getEntitiesBranchListUrl: "http://localhost:44339/api/entities/branch/getall",
  getBankListUrl: "http://localhost:44339/api/entities/branch/bank/getall",
  getClientRegUrl: "http://localhost:44339/api/clients/register",
  modifyClientUrl: "http://localhost:44339/api/clients/modify",
  getClientUrl: "http://localhost:44339/api/clients",
  getEntityUrl: "http://localhost:44339/api/entities",
  getBranchUrl: "http://localhost:44339/api/entities/branch",
  getClientEntityUrl: "http://localhost:44339/api/entities/create",
  getEntityBranchUrl: "http://localhost:44339/api/entities/branch/create",
  submitBankUrl: "http://localhost:44339/api/entities/branch/bank/create",
  modifyBankUrl: "http://localhost:44339/api/entities/branch/bank/modify",
  modifyClientEntityUrl: "http://localhost:44339/api/entities/modify",
  modifyEntityBranchUrl: "http://localhost:44339/api/entities/branch/modify",
  getQuotationListUrl: "http://localhost:44339/api/quotation/getquotations",
  getConsultants:"http://localhost:44339/api/users/getconsultants",
  getServiceById:"http://localhost:44339/api/service/{id}",
  deactivateEntity : "http://localhost:44339/api/entities/deactivate",
  activateEntity : "http://localhost:44339/api/entities/reactivate",
  deleteEntity:"http://localhost:44339/api/entities/branch/{id}/delete",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
